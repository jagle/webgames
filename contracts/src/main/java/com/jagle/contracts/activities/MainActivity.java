package com.jagle.contracts.activities;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import com.jagle.contracts.biz.Property;
import com.jagle.contracts.data.Properties;
import com.jagle.contracts.data.Tables;
import com.jagle.kit.base.JListAdapter;
import com.jagle.kit.base.JListAdapterEx;
import com.jagle.kit.base.JUiActivity;
import com.jagle.contracts.R;
import com.jagle.kit.common.ViewRef;
import com.jagle.kit.property.Binding;
import com.jagle.threesdk.AsyncImageView;
import com.umeng.message.PushAgent;
import com.umeng.update.UmengUpdateAgent;

public class MainActivity extends JUiActivity {

    private DigestListAdapter mAdapter;
    private ViewRef<ListView> mList = new ViewRef<ListView>(this, R.id.am_list);
    private MenuItem mSearchItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAdapter = new DigestListAdapter(this);
        UmengUpdateAgent.update(this);
        PushAgent.getInstance(this).onAppStart();
        PushAgent.getInstance(this).enable();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        mSearchItem = menu.findItem(R.id.search);
        SearchView searchView =  (SearchView) mSearchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.filter(newText);
                return true;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    @Override
    public int getContentViewId() {
        return R.layout.activity_main;
    }


    @Override
    protected void onCreateContentView() {
        super.onCreateContentView();
        mList.get().setAdapter(mAdapter);
        mList.get().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Tables.Contract contract = mAdapter.getDatas().get(i);
                ContractAcitvity.show(MainActivity.this, contract.path, contract.name);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        Binding.bind(Properties.pContracts, mAdapter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Binding.unbind(Properties.pContracts, mAdapter);
    }

    @Override
    public void onBackPressed() {
        if (MenuItemCompat.isActionViewExpanded(mSearchItem)){
            MenuItemCompat.collapseActionView(mSearchItem);
        } else {
            super.onBackPressed();
        }
    }

    private static class DigestListAdapter extends JListAdapterEx<Tables.Contract> {
        public DigestListAdapter(Context context) {
            super(context, R.layout.item_contract_list);
        }

        @Override
        protected void onItemCreated(final View convertView) {
            super.onItemCreated(convertView);
        }

        @Override
        public void updateView(View view, Tables.Contract data) {
            ((TextView) view).setText(data.name);
        }

        @Override
        public String getDataString(Tables.Contract data) {
            return data.name;
        }
    }
}
