package com.jagle.contracts.biz;

import android.text.TextUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jagle.contracts.App;
import com.jagle.contracts.data.Jsons;
import com.jagle.contracts.data.Properties;
import com.jagle.contracts.data.Tables;
import com.jagle.kit.common.Developer;
import com.jagle.kit.common.JLog;
import com.jagle.kit.common.JToast;
import com.jagle.kit.property.ValueProperty;
import com.jagle.kit.utils.JFileUtils;
import com.jagle.kit.utils.JTimeUtils;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;

import cz.msebera.android.httpclient.Header;

/**
 * Created by jagle on 15/10/28.
 */
public class Property {
    public static final String URL = "http://7xnuwi.com1.z0.glb.clouddn.com";

    private static Property mInstance;

    private Property() {

    }

    public static Property getInstance() {
        if (mInstance == null) {
            mInstance = new Property();
        }

        return mInstance;
    }

    public void downloadContracts() {
        JLog.d(Developer.Jagle, "downloadGroup");
        final String contractUrl = URL + "/jsons/contracts.json";
        if (checkUrlInCache(contractUrl)) {
            return;
        }

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(contractUrl, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                ObjectMapper mapper = new ObjectMapper();
                try {
                    Jsons.ContractJson json = mapper.readValue(responseBody, Jsons.ContractJson.class);
                    ArrayList<Tables.Contract> tgroups = new ArrayList<Tables.Contract>(json.contracts.size());
                    for (Jsons.Contract group : json.contracts) {
                        tgroups.add(JsonToTable.translate(group));
                    }
                    Properties.pContracts.set(tgroups);
                    Properties.pJsonInfo.add(JsonToTable.translate(json.jsonInfo, contractUrl));
                } catch (IOException e) {
                    JLog.e(Developer.Jagle, "parse groups failed, ", e);
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                JLog.e(Developer.Jagle, "load groups failed, ", error);
                JToast.show("网络异常");
            }
        });
    }

    public boolean checkUrlInCache(String url) {
        Tables.JsonInfo info = Properties.pJsonInfo.getValue(url);
        if (info == null) {
            return false;
        }

        if (info.updatetime == null) {
            return false;
        }

        try {
            if (JTimeUtils.diffNowInDays(info.updatetime) < info.liefCycle) {
                JLog.i(Developer.Jagle, "json data in local, update time:" + info.updatetime + "url, " + url);
                return true;
            }
        } catch (ParseException e) {
            return false;
        }
        return false;
    }

    public void readContractFromDisk(String path, JFileUtils.TextReadCallback callback) {
        File dir = App.getApp().getExternalFilesDir("contracts");
        String filePath = dir.getPath() + "/" + path;
        JFileUtils.asyncReadTextFile(filePath, callback);
    }

    public void writeContractToDisk(String path, String text) {
        File dir = App.getApp().getExternalFilesDir("contracts");
        String filePath = dir.getPath() + "/" + path;
        JFileUtils.asyncWriteFile(filePath, text);
    }

    public ValueProperty<String> getContract(String path) {
        ValueProperty<String> data = Properties.pContractCache.get(path);
        if (data == null) {
            data = new ValueProperty<>("");
            Properties.pContractCache.put(path, data);
        }
        return data;
    }

    public void loadContract(final String path) {
        if (!TextUtils.isEmpty(getContract(path).get())) {
            return;
        }

        readContractFromDisk(path, new JFileUtils.TextReadCallback() {
            @Override
            public void onFinish(String text) {
                if (!TextUtils.isEmpty(text)) {
                    getContract(path).set(text);
                    return;
                } else {
                    loadContractFromNet(path);
                }
            }
        });


    }

    private void loadContractFromNet(final String path) {
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(URL + "/contracts/" + path, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                JLog.d(Developer.Jagle, new String(responseBody));
                String text = new String(responseBody);
                getContract(path).set(text);
                writeContractToDisk(path, text);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                JLog.e(Developer.Jagle, "load contract failed, " + path, error);
            }
        });
    }
}
