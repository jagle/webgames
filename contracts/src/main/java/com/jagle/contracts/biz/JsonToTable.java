package com.jagle.contracts.biz;

import com.jagle.contracts.data.Jsons;
import com.jagle.contracts.data.Tables;
import com.jagle.kit.utils.JTimeUtils;

/**
 * Created by jagle on 15/10/28.
 */
public class JsonToTable {
    public static Tables.Contract translate(Jsons.Contract json){
        Tables.Contract table = new Tables.Contract();
        table.name = json.name;
        table.path = json.path;
        return table;
    }

    public static Tables.JsonInfo translate(Jsons.JsonInfo json, String url){
        Tables.JsonInfo table = new Tables.JsonInfo();
        table.url = url;
        table.name = json.name;
        table.version = json.version;
        table.timestamp = json.timestamp;
        table.liefCycle = json.liefCycle;
        table.updatetime = JTimeUtils.currentTime();
        return table;
    }
}
