package com.jagle.contracts.biz;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.jagle.contracts.data.Tables;
import com.jagle.kit.common.Developer;
import com.jagle.kit.common.JLog;
import com.jagle.threesdk.JOrmliteHelper;

import java.sql.SQLException;

/**
 * Created by jagle on 15/8/18.
 */
public class OrmliteHelper extends JOrmliteHelper {

    // name of the database file for your application -- change to something
    // appropriate for your app
    private static final String DATABASE_NAME = "contracts.db";
    // any time you make changes to your database objects, you may have to
    // increase the database version
    private static final int DATABASE_VERSION = 1;

    public OrmliteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        super.onCreate(database, connectionSource);
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        super.onUpgrade(database, connectionSource, oldVersion, newVersion);
    }

    @Override
    protected void initTables() {
        try {
            TableUtils.createTableIfNotExists(connectionSource, Tables.Contract.class);
            TableUtils.createTableIfNotExists(connectionSource, Tables.JsonInfo.class);
        } catch (SQLException e) {
            JLog.e(Developer.Jagle, "failed to create tables");
            return;
        }
    }
}
