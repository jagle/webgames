package com.jagle.contracts.data;

import android.util.LruCache;

import com.jagle.kit.property.ListProperty;
import com.jagle.kit.property.ValueProperty;
import com.jagle.threesdk.DBListProperty;

import java.util.HashMap;

/**
 * Created by jagle on 15/10/28.
 */
public class Properties {
    public final static DBListProperty<Tables.JsonInfo, String> pJsonInfo =
            new DBListProperty<Tables.JsonInfo, String>(Tables.JsonInfo.class) {
                @Override
                public String getKey(Tables.JsonInfo data) {
                    return data.url;
                }
            };

    public final static DBListProperty<Tables.Contract, String> pContracts = new DBListProperty<Tables.Contract, String>(Tables.Contract.class) {
        @Override
        public String getKey(Tables.Contract data) {
            return data.name;
        }
    };

    public final static LruCache<String, ValueProperty<String>> pContractCache = new LruCache<>(4 * 1024 * 1024);
}
