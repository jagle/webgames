package com.jagle.contracts.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;

/**
 * Created by jagle on 15/10/28.
 */
public class Jsons {
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class JsonInfo{
        public String name;
        public int version;
        public String timestamp;
        public int liefCycle;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Contract{
        public String name;
        public String path;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class ContractJson{
        public JsonInfo jsonInfo;
        public ArrayList<Contract> contracts;
    }

}
