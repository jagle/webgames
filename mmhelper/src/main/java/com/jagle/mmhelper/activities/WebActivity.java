package com.jagle.mmhelper.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.DownloadListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.jagle.kit.base.JUiActivity;
import com.jagle.kit.common.ViewRef;
import com.jagle.kit.utils.JActivityUtils;
import com.jagle.kit.utils.JUI;
import com.jagle.mmhelper.R;
import com.jagle.mmhelper.biz.Property;

/**
 * Created by jagle on 15/8/24.
 */
public class WebActivity extends JUiActivity {
    public static final String DATA_TITLE = "data.title";
    public static final String DATA_URL = "data.url";

    ViewRef<WebView> mWebView = new ViewRef<WebView>(this, R.id.aw_webview);
    ViewRef<ProgressBar> mProgressBar = new ViewRef<ProgressBar>(this, R.id.aw_progress);

    private String mTitle;
    private String mUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mTitle = getIntent().getStringExtra(DATA_TITLE);
        mUrl = getIntent().getStringExtra(DATA_URL);

        setTitle(mTitle);
    }

    @Override
    protected void onCreateContentView() {
        super.onCreateContentView();

        if (Build.VERSION.SDK_INT >= 21){
            CookieManager.getInstance().setAcceptThirdPartyCookies(mWebView.get(), true);
        }
        CookieManager.getInstance().setAcceptCookie(true);

        mWebView.get().setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                // TODO Auto-generated method stub
                view.loadUrl(url);
                return true;
            }

        });

        mWebView.get().setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
                mProgressBar.get().setProgress(newProgress);
                mProgressBar.setVisibility(JUI.getVisibleOrGone(newProgress != 100));
            }
        });

        mWebView.get().setDownloadListener(new MyWebViewDownLoadListener());

        mWebView.get().getSettings().setJavaScriptEnabled(true);
        mWebView.get().loadUrl(mUrl);
    }

    @Override
    public int getContentViewId() {
        return R.layout.activity_web;
    }

    private class MyWebViewDownLoadListener implements DownloadListener {

        @Override
        public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype,
                                    long contentLength) {
            Uri uri = Uri.parse(url);
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (mWebView.get() == null) {
            return super.onKeyDown(keyCode, event);
        }

        if (mWebView.get().canGoBack() && event.getKeyCode() == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            mWebView.get().goBack();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    public static void show(Activity act, String title, String url){
        Bundle b = new Bundle();
        b.putString(WebActivity.DATA_TITLE, title);
        b.putString(WebActivity.DATA_URL, url);
        JActivityUtils.jump(act, WebActivity.class, b);
    }

    public static void synCookies(Context context, String url, String cookies) {

        //cookies是在HttpClient中获得的cookie
    }
}
