package com.jagle.mmhelper.activities;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.jagle.kit.base.JListAdapter;
import com.jagle.kit.base.JUiActivity;
import com.jagle.kit.common.ViewRef;
import com.jagle.kit.property.Binding;
import com.jagle.kit.utils.JActivityUtils;
import com.jagle.mmhelper.R;
import com.jagle.mmhelper.biz.Property;
import com.jagle.mmhelper.data.Tables;
import com.jagle.threesdk.AsyncImageView;

/**
 * Created by jagle on 15/8/18.
 */
public class PNListActivty extends JUiActivity {
    public  static final String DATA_GROUP_ID = "data.group.id";

    private PNListAdapter mAdapter;
    private ViewRef<ListView> mList = new ViewRef<ListView>(this, R.id.mpn_list);
    private String mGroupId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAdapter = new PNListAdapter(this);
        mGroupId = getIntent().getStringExtra(DATA_GROUP_ID);
        Property.getInstance().downloadGroup(mGroupId, null);
    }

    @Override
    public int getContentViewId() {
        return R.layout.activity_pn_list;
    }

    @Override
    protected void onCreateContentView() {
        super.onCreateContentView();
        mList.get().setAdapter(mAdapter);
        mList.get().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                DetailActivity.show(PNListActivty.this, mAdapter.getDatas().get(i).orderId, mGroupId);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        Binding.bind(Property.getInstance().getGroup(mGroupId), mAdapter);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mGroupId = savedInstanceState.getString(DATA_GROUP_ID);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(DATA_GROUP_ID, mGroupId);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Binding.unbind(Property.getInstance().getGroup(mGroupId), mAdapter);
    }

    private static class PNListAdapter extends JListAdapter<Tables.MMPN>{
        public PNListAdapter(Context context){
            super(context, R.layout.item_pn);
        }

        @Override
        protected void onItemCreated(final View convertView) {
            super.onItemCreated(convertView);

            JViewHolder<Tables.MMPN> holder = new JViewHolder<Tables.MMPN>(){
                private AsyncImageView mImage = (AsyncImageView)convertView.findViewById(R.id.ipn_image) ;
                private TextView mLabel = (TextView)convertView.findViewById(R.id.ipn_label);
                private TextView mDesc = (TextView)convertView.findViewById(R.id.ipn_description);

                @Override
                public void updateView(int position, Tables.MMPN data) {
                    mImage.setImageURI(data.imageUrl);
                    mLabel.setText(data.label);
                    mDesc.setText(data.description);
                }
            };
            convertView.setTag(holder);
        }

        @Override
        public void updateView(View view, Tables.MMPN data) {
            ((JViewHolder<Tables.MMPN>) view.getTag()).updateView(0, data);
        }
    }
}
