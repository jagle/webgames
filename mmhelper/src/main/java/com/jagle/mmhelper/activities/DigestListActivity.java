package com.jagle.mmhelper.activities;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.jagle.kit.base.JListAdapter;
import com.jagle.kit.base.JUiActivity;
import com.jagle.kit.common.ViewRef;
import com.jagle.kit.property.Binding;
import com.jagle.kit.utils.JActivityUtils;
import com.jagle.mmhelper.R;
import com.jagle.mmhelper.biz.Property;
import com.jagle.mmhelper.data.Tables;
import com.jagle.threesdk.AsyncImageView;

/**
 * Created by jagle on 15/9/14.
 */
public class DigestListActivity extends JUiActivity{

    public  static final String DATA_MMID = "data.mmid";

    private DigestListAdapter mAdapter;
    private ViewRef<ListView> mList = new ViewRef<ListView>(this, R.id.adl_list);
    private String mmid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAdapter = new DigestListAdapter(this);
        mmid = getIntent().getStringExtra(DATA_MMID);
    }

    @Override
    public int getContentViewId() {
        return R.layout.activity_digest_list;
    }

    @Override
    protected void onCreateContentView() {
        super.onCreateContentView();
        mList.get().setAdapter(mAdapter);
        mList.get().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Tables.Digest d = mAdapter.getDatas().get(i);
                String url = "http://weixin.sogou.com" + d.url;
                Property.getInstance().storeCookie(url, mmid);
                WebActivity.show(DigestListActivity.this, d.title1, url);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        Binding.bind(Property.getInstance().getDigest(mmid), mAdapter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Binding.unbind(Property.getInstance().getDigest(mmid), mAdapter);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mmid = savedInstanceState.getString(DATA_MMID);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(DATA_MMID, mmid);
    }

    private static class DigestListAdapter extends JListAdapter<Tables.Digest> {
        public DigestListAdapter(Context context){
            super(context, R.layout.item_digest);
        }

        @Override
        protected void onItemCreated(final View convertView) {
            super.onItemCreated(convertView);

            JViewHolder<Tables.Digest> holder = new JViewHolder<Tables.Digest>(){
                private AsyncImageView mImage = (AsyncImageView)convertView.findViewById(R.id.id_image) ;
                private TextView mDate = (TextView)convertView.findViewById(R.id.id_date);
                private TextView mTitle = (TextView)convertView.findViewById(R.id.id_title);
                private TextView mContent = (TextView)convertView.findViewById(R.id.id_content);

                @Override
                public void updateView(int position, Tables.Digest data) {
                    mImage.setImageURI(data.imglink);
                    mDate.setText(data.date);
                    mTitle.setText(data.title1);
                    mContent.setText(data.content168);
                }
            };
            convertView.setTag(holder);
        }

        @Override
        public void updateView(View view, Tables.Digest data) {
            ((JViewHolder<Tables.Digest>) view.getTag()).updateView(0, data);
        }
    }
}
