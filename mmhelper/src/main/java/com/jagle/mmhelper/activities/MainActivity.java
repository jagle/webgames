package com.jagle.mmhelper.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.jagle.kit.base.JUIActivityV4;
import com.jagle.kit.base.JListPagerAdapter;
import com.jagle.kit.common.Developer;
import com.jagle.kit.common.JLog;
import com.jagle.kit.common.ViewRef;
import com.jagle.kit.property.Binding;
import com.jagle.kit.utils.JActivityUtils;
import com.jagle.mmhelper.R;
import com.jagle.mmhelper.data.SProperties;
import com.jagle.mmhelper.data.Tables;
import com.umeng.message.IUmengRegisterCallback;
import com.umeng.message.PushAgent;
import com.umeng.message.UmengRegistrar;
import com.umeng.update.UmengUpdateAgent;

public class MainActivity extends JUIActivityV4 {

    private GroupPagerAdapter mAdapter;
    private PushAgent mPushAgent;

    private ViewRef<ViewPager> mPager = new ViewRef<ViewPager>(this, R.id.ma_pager);
    private ViewRef<PagerTabStrip> mTabs = new ViewRef<PagerTabStrip>(this, R.id.ma_tabs);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        UmengUpdateAgent.update(this);
        mAdapter = new GroupPagerAdapter(getSupportFragmentManager());

        mPushAgent = PushAgent.getInstance(this);
        mPushAgent.onAppStart();
        mPushAgent.enable(mRegisterCallback);

    }

    public IUmengRegisterCallback mRegisterCallback = new IUmengRegisterCallback() {

        @Override
        public void onRegistered(String registrationId) {
            String device_token = UmengRegistrar.getRegistrationId(getContext());
            JLog.d(Developer.Jagle, "device_token:" + device_token);
        }
    };

    @Override
    protected void onCreateContentView() {
        super.onCreateContentView();
        mPager.get().setAdapter(mAdapter);
        mTabs.get().setTabIndicatorColorResource(R.color.red);
        mTabs.get().setTextColor(Color.RED);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Binding.bind(SProperties.pMMGroups, mAdapter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Binding.unbind(SProperties.pMMGroups, mAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_feedback:
                FeedbackActivity.feedback(this);
                return true;
            case R.id.action_about_us:
                JActivityUtils.jump(this, AboutUsActiviy.class);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean isRootActivity() {
        return true;
    }

    @Override
    public int getContentViewId() {
        return R.layout.activity_main;
    }

    class GroupPagerAdapter extends JListPagerAdapter<Tables.MMGroup>{
        public GroupPagerAdapter(FragmentManager fm) {
            super(fm, LastDigestFragment.class);
        }

        @Override
        public Bundle getArguments(Tables.MMGroup data) {
            return LastDigestFragment.buildArguments(data.typeId);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return getData(position).label;
        }
    }
}
