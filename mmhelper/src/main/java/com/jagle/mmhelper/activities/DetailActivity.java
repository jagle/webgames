package com.jagle.mmhelper.activities;

import android.app.Activity;
import android.content.ClipData;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.jagle.kit.base.JUiActivity;
import com.jagle.kit.common.JToast;
import com.jagle.kit.common.ViewRef;
import com.jagle.kit.utils.JActivityUtils;
import com.jagle.mmhelper.R;
import com.jagle.mmhelper.biz.Property;
import com.jagle.mmhelper.data.Tables;
import com.jagle.threesdk.AsyncImageView;

import java.util.Locale;

/**
 * Created by jagle on 15/8/20.
 */
public class DetailActivity extends JUiActivity {
    public static final String DATA_ID = "data.mmid";
    public static final String DATA_GROUP = "data.group";

    private String mMMIDStr ;
    private String mGroupId;

    private ViewRef<TextView> mLabel = new ViewRef<TextView>(this, R.id.ad_label);
    private ViewRef<TextView> mMMID = new ViewRef<TextView>(this, R.id.ad_id);
    private ViewRef<TextView> mDesc = new ViewRef<TextView>(this, R.id.ad_description);
    private ViewRef<TextView> mWebUrl = new ViewRef<TextView>(this, R.id.ad_web);
    private ViewRef<TextView> mWeiboUrl = new ViewRef<TextView>(this, R.id.ad_weibo);
    private ViewRef<AsyncImageView> mImage = new ViewRef<AsyncImageView>(this, R.id.ad_image);
    private ViewRef<View> mLayoutWeibo = new ViewRef<View>(this, R.id.ad_layout_weibo);
    private ViewRef<View> mLayoutWeb = new ViewRef<View>(this, R.id.ad_layout_web);
    private ViewRef<View> mViewHistory = new ViewRef<View>(this, R.id.ad_view_history);

    @Override
    public int getContentViewId() {
        return R.layout.activity_detail;
    }

    @Override
    protected void onCreateContentView() {
        String id = getIntent().getStringExtra(DATA_ID);
        mGroupId = getIntent().getStringExtra(DATA_GROUP);

        Tables.MMPN pn = Property.getInstance().getGroup(mGroupId).getValue(id);
        mLabel.get().setText(pn.label);
        mDesc.get().setText(pn.description);
        mMMID.get().setText(pn.mmid);
        mWebUrl.get().setText(pn.webUrl);
        mWeiboUrl.get().setText(pn.weiboUrl);
        mImage.get().setImageURI(pn.imageUrl);

        mMMIDStr = pn.mmid;

        if (TextUtils.isEmpty(pn.webUrl)){
            mLayoutWeb.get().setClickable(false);
        }

        if (TextUtils.isEmpty(pn.weiboUrl)){
            mLayoutWeibo.get().setClickable(false);
        }

        if (TextUtils.isEmpty(pn.openid)){
            mViewHistory.setVisibility(View.GONE);
        }
    }

    public void onWeb(View view) {
        Bundle b = new Bundle();
        b.putString(WebActivity.DATA_TITLE, mLabel.get().getText().toString());
        b.putString(WebActivity.DATA_URL, mWebUrl.get().getText().toString());
        JActivityUtils.jump(this, WebActivity.class, b);
    }

    public void onWeibo(View view) {
        Bundle b = new Bundle();
        b.putString(WebActivity.DATA_TITLE, mLabel.get().getText().toString());
        b.putString(WebActivity.DATA_URL, mWeiboUrl.get().getText().toString());
        JActivityUtils.jump(this, WebActivity.class, b);
    }

    public void onCopy(View view) {
        android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("MMID", mMMIDStr);
        clipboard.setPrimaryClip(clip);
        String fmt = getString(R.string.toast_clip_mmid);
        JToast.show(String.format(Locale.getDefault(), fmt, mMMIDStr));
    }

    public void onDigest(View view){
        Bundle b = new Bundle();
        b.putString(DigestListActivity.DATA_MMID, mMMIDStr);
        b.putString(JUiActivity.DATA_ACTIVITY_TITLE, mLabel.get().getText().toString());
        JActivityUtils.jump(this, DigestListActivity.class, b);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public static void show(Activity act, String orderId, String groupid ){
        Bundle b = new Bundle();
        b.putString(DetailActivity.DATA_ID, orderId);
        b.putString(DetailActivity.DATA_GROUP, groupid);
        JActivityUtils.jump(act, DetailActivity.class, b);
    }

}
