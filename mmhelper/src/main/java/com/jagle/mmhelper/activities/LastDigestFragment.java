package com.jagle.mmhelper.activities;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.jagle.kit.base.JListAdapter;
import com.jagle.kit.base.JUIFragmentV4;
import com.jagle.kit.common.ViewRef;
import com.jagle.kit.property.Binding;
import com.jagle.mmhelper.R;
import com.jagle.mmhelper.biz.DigestHub;
import com.jagle.mmhelper.biz.Property;
import com.jagle.mmhelper.data.Digest;
import com.jagle.threesdk.AsyncImageView;

/**
 * Created by jagle on 15/9/17.
 */
public class LastDigestFragment extends JUIFragmentV4 {
    public static  final String DATA_GROUP_ID = "data.group.id";

    public static Bundle buildArguments(String groupId){
        Bundle b = new Bundle();
        b.putString(DATA_GROUP_ID, groupId);
        return  b;
    }

    private DigestListAdapter mAdapter;
    private ViewRef<ListView> mList;
    private String mGroupId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mGroupId = getArguments().getString(DATA_GROUP_ID);
        mAdapter = new DigestListAdapter(getActivity());
        DigestHub.getInstance().listenerGroup(mGroupId);
    }

    @Override
    protected void onCreateContentView(View view) {
        mList = new ViewRef<ListView>(view, R.id.fdl_list);

        mList.get().setAdapter(mAdapter);
        mList.get().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Digest d = mAdapter.getDatas().get(i);
                DigestActivity.show(getActivity(), mGroupId, d.orderid, "http://weixin.sogou.com" + d.url);
            }
        });
    }

    @Override
    public int getContentViewId() {
        return R.layout.fragment_digest_list;
    }

    @Override
    public void onResume() {
        super.onResume();
        Property.getInstance().downloadGroup(mGroupId, null);

        Binding.bind(Property.getInstance().getLastDigest(mGroupId), mAdapter);
    }

    @Override
    public void onPause() {
        super.onPause();
        Binding.unbind(Property.getInstance().getLastDigest(mGroupId), mAdapter);
    }

    private static class DigestListAdapter extends JListAdapter<Digest> {
        public DigestListAdapter(Context context){
            super(context, R.layout.item_digest);
        }

        @Override
        protected void onItemCreated(final View convertView) {
            super.onItemCreated(convertView);

            JViewHolder<Digest> holder = new JViewHolder<Digest>(){
                private AsyncImageView mImage = (AsyncImageView)convertView.findViewById(R.id.id_image) ;
                private TextView mDate = (TextView)convertView.findViewById(R.id.id_date);
                private TextView mTitle = (TextView)convertView.findViewById(R.id.id_title);
                private TextView mContent = (TextView)convertView.findViewById(R.id.id_content);

                @Override
                public void updateView(int position, Digest data) {
                    mImage.setImageURI(data.imageUrl);
                    mDate.setText(data.date);
                    mTitle.setText(data.title);
                    mContent.setText(data.content);
                }
            };
            convertView.setTag(holder);
        }

        @Override
        public void updateView(View view, Digest data) {
            ((JViewHolder<Digest>) view.getTag()).updateView(0, data);
        }
    }
}
