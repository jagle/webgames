package com.jagle.mmhelper.activities;

import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.jagle.kit.base.JListAdapter;
import com.jagle.kit.base.JUiActivity;
import com.jagle.kit.common.Developer;
import com.jagle.kit.common.JLog;
import com.jagle.kit.common.ViewRef;
import com.jagle.kit.property.Binding;
import com.jagle.kit.utils.JActivityUtils;
import com.jagle.mmhelper.R;
import com.jagle.mmhelper.data.SProperties;
import com.jagle.mmhelper.data.Tables;
import com.jagle.threesdk.AsyncImageView;
import com.umeng.message.IUmengRegisterCallback;
import com.umeng.message.PushAgent;
import com.umeng.message.UmengRegistrar;
import com.umeng.update.UmengUpdateAgent;

public class MainActivity1 extends JUiActivity {

    private GroupListAdapter mAdapter;
    private PushAgent mPushAgent;

    private ViewRef<GridView> mGrid = new ViewRef<GridView>(this, R.id.ma_grid);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        UmengUpdateAgent.update(this);

        mAdapter = new GroupListAdapter(this);

        mPushAgent = PushAgent.getInstance(this);
        mPushAgent.onAppStart();
        mPushAgent.enable(mRegisterCallback);

    }

    public IUmengRegisterCallback mRegisterCallback = new IUmengRegisterCallback() {

        @Override
        public void onRegistered(String registrationId) {
            // TODO Auto-generated method stub
            String device_token = UmengRegistrar.getRegistrationId(getContext());
            JLog.e(Developer.Jagle, "device_token:" + device_token);
        }
    };


    @Override
    protected void onCreateContentView() {
        super.onCreateContentView();
        mGrid.get().setAdapter(mAdapter);
        mGrid.get().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Bundle b = new Bundle();
                b.putString(JUiActivity.DATA_ACTIVITY_TITLE, SProperties.pMMGroups.get().get(i).label);
                b.putString(PNListActivty.DATA_GROUP_ID, SProperties.pMMGroups.get().get(i).typeId);
                JActivityUtils.jump(MainActivity1.this, PNListActivty.class, b);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Binding.bind(SProperties.pMMGroups, mAdapter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Binding.unbind(SProperties.pMMGroups, mAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_feedback:
                FeedbackActivity.feedback(this);
                return true;
            case R.id.action_about_us:
                JActivityUtils.jump(this, AboutUsActiviy.class);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean isRootActivity() {
        return true;
    }

    @Override
    public int getContentViewId() {
        return R.layout.activity_main;
    }


    public class GroupListAdapter extends JListAdapter<Tables.MMGroup>{

        public GroupListAdapter(Context context) {
            super(context, R.layout.item_groud);
        }


        @Override
        protected void onItemCreated(final View convertView) {
            super.onItemCreated(convertView);

            JViewHolder<Tables.MMGroup> holder = new JViewHolder<Tables.MMGroup>(){
                private AsyncImageView mImage = (AsyncImageView)convertView.findViewById(R.id.ig_image) ;
                private TextView mLabel = (TextView)convertView.findViewById(R.id.ig_label);

                @Override
                public void updateView(int position, Tables.MMGroup data) {
                    mImage.setImageURI(data.imageUrl);
                    mLabel.setText(data.label);
                }
            };
            convertView.setTag(holder);
        }

        @Override
        public void updateView(View view, Tables.MMGroup data) {
            ((JViewHolder<Tables.MMGroup>) view.getTag()).updateView(0, data);
        }
    }


}
