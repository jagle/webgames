package com.jagle.mmhelper;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.jagle.kit.base.JApplication;
import com.jagle.kit.common.Developer;
import com.jagle.kit.common.JLog;
import com.jagle.kit.common.JToast;
import com.jagle.kit.utils.JUI;
import com.jagle.mmhelper.activities.FeedbackActivity;
import com.jagle.mmhelper.biz.DigestHub;
import com.jagle.mmhelper.biz.OrmliteHelper;
import com.jagle.mmhelper.biz.Property;
import com.jagle.mmhelper.biz.SogouAes;
import com.jagle.threesdk.AsyncImageView;
import com.umeng.fb.push.FeedbackPush;
import com.umeng.message.PushAgent;

/**
 * Created by jagle on 15/8/17.
 */
public class App extends JApplication{

    private PushAgent mPushAgent;

    @Override
    public void onCreate() {
        super.onCreate();

        OpenHelperManager.getHelper(this, OrmliteHelper.class);
        AsyncImageView.initConfig(this);
        Property.getInstance().init();
        DigestHub.getInstance();

        FeedbackPush.getInstance(this).init(FeedbackActivity.class, true);

        mPushAgent = PushAgent.getInstance(this);
        mPushAgent.setDebugMode(BuildConfig.DEBUG);
    }

    @Override
    public boolean isDebug() {
        return BuildConfig.DEBUG;
    }
}
