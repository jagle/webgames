package com.jagle.mmhelper.biz;

import android.os.Handler;

import com.jagle.kit.common.Developer;
import com.jagle.kit.common.JLog;
import com.jagle.kit.property.Binding;
import com.jagle.kit.property.DataCenter;
import com.jagle.kit.property.IListPropertyListener;
import com.jagle.mmhelper.data.Digest;
import com.jagle.mmhelper.data.Tables;

import java.util.HashMap;
import java.util.List;

/**
 * Created by jagle on 15/9/17.
 */
public class DigestHub {
    private HashMap<String, HashMap<String, Digest>> mapDigestMap = new HashMap<>();
    private HashMap<String, MMPNListValueListener> mmpnListValueListenerHashMap = new HashMap<>();
    private long mLastDownDigestTime;
    private static final long DOWNLOAD_DURATION = 200;

    private static DigestHub mInstance;

    private DigestHub(){
    }

    public static DigestHub getInstance() {
        if (mInstance == null) {
            mInstance = new DigestHub();
        }

        return mInstance;
    }

    public void loadLocalDigest(final String groupId, List<Tables.MMPN> data) {
        if(data == null){
            return;
        }

        for(Tables.MMPN mmpn : data){
            Property.getInstance().getDigest(mmpn.mmid);
        }
        DataCenter.post(new Runnable() {
            @Override
            public void run() {
                Property.getInstance().updateLastDigests(groupId);
            }
        });
    }

    public synchronized HashMap<String, Digest> getDigestMap(String groupId){
        HashMap<String, Digest> map = mapDigestMap.get(groupId);
        if(map == null){
            map = new HashMap<>();
            mapDigestMap.put(groupId, map);
        }
        return map;
    }

    public synchronized MMPNListValueListener getMMPNListValueListener(final String groupId){
        MMPNListValueListener listener = mmpnListValueListenerHashMap.get(groupId);
        if(listener == null){
            listener = new MMPNListValueListener(groupId);
            mmpnListValueListenerHashMap.put(groupId, listener);
        }
        return listener;
    }

    public void listenerGroup(String groupId){
        Binding.bind(Property.getInstance().getGroup(groupId), getMMPNListValueListener(groupId) );
    }

    public synchronized void downloadMMPNS(final String groupid, final List<Tables.MMPN> list, final int index){
        long now = System.currentTimeMillis();
        if (now - mLastDownDigestTime < DOWNLOAD_DURATION){
            JLog.d(Developer.Jagle, "post delay downloadMMPNS:"+ groupid + ", " + index);
            Handler h = new Handler();
            h.postDelayed(new Runnable() {
                @Override
                public void run() {
                    downloadMMPNS(groupid, list, index);
                }
            }, DOWNLOAD_DURATION);
            return;
        }
        mLastDownDigestTime = now;

        if (list == null){
            return;
        }


        if (index >= list.size()){
            DataCenter.post(new Runnable() {
                @Override
                public void run() {
                    Property.getInstance().updateLastDigests(groupid);
                }
            });
            return;
        }

        JLog.d(Developer.Jagle, "downloadMMPNS:"+ groupid + ", " + list.size() + ", " + index);
        Property.getInstance().downloadDigests(list.get(index), new Property.AsyncCallback() {
            @Override
            public void onFinish() {
                 downloadMMPNS(groupid, list, index + 1);
            }
        });
    }

    public class MMPNListValueListener implements IListPropertyListener<Tables.MMPN>{
        String mGroupid;

        public MMPNListValueListener(String groupId){
            mGroupid = groupId;
        }

        @Override
        public void onDataChanged(List<Tables.MMPN> data) {
            loadLocalDigest(mGroupid, data);
            downloadMMPNS(mGroupid, data, 0);
        }
    }

}
