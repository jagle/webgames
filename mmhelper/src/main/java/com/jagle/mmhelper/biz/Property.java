package com.jagle.mmhelper.biz;

import android.text.TextUtils;
import android.webkit.CookieManager;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.jagle.kit.common.Developer;
import com.jagle.kit.common.JLog;
import com.jagle.kit.property.DataCenter;
import com.jagle.kit.property.ListProperty;
import com.jagle.kit.utils.JTimeUtils;
import com.jagle.mmhelper.App;
import com.jagle.mmhelper.data.Digest;
import com.jagle.mmhelper.data.Jsons;
import com.jagle.mmhelper.data.SProperties;
import com.jagle.mmhelper.data.Tables;
import com.jagle.mmhelper.data.Xmls;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;

import org.apache.http.cookie.Cookie;
import org.apache.http.impl.cookie.BasicClientCookie;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * Created by jagle on 15/8/17.
 */
public class Property {

    private static final String GROUPS_URL = "http://7xl6ip.com1.z0.glb.clouddn.com/jsons/groups.json";

    public static final String URL = "http://weixin.sogou.com/gzhjs";

    private static final String EXT_VALUE = "alQlAlGoElkhuUZpeeFX2Oz4j--5XZYgdGuvtPIqu0V3GWWV6fSKw8wJH_fPvHBt";

    private static Property mInstance;

    private HashMap<String, PersistentCookieStore> mCookies = new HashMap<>();

    private Property() {

    }

    public static Property getInstance() {
        if (mInstance == null) {
            mInstance = new Property();
        }

        return mInstance;
    }


    public void init() {
        SProperties.init();
        DataCenter.post(new Runnable() {
            @Override
            public void run() {
                downloadGroups(null);
            }
        });
    }

    public void downloadGroups(final AsyncCallback callback) {
        JLog.d(Developer.Jagle, "downloadGroup");
        if (checkUrlInCache(GROUPS_URL)) {
            if (callback != null){
                callback.onFinish();
            }
            return;
        }

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(GROUPS_URL, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                ObjectMapper mapper = new ObjectMapper();
                try {
                    Jsons.MMGroups groups = mapper.readValue(responseBody, Jsons.MMGroups.class);
                    ArrayList<Tables.MMGroup> tgroups = new ArrayList<Tables.MMGroup>(groups.groups.size());
                    for (Jsons.MMGroup group : groups.groups) {
                        tgroups.add(JsonToTable.transMMGroup(group));
                    }
                    SProperties.pMMGroups.set(tgroups);
                    SProperties.pJsonInfo.add(JsonToTable.transJsonInfo(GROUPS_URL, groups.jsonInfo));
                    SProperties.pMMPNListMap.clear();
                } catch (IOException e) {
                    JLog.e(Developer.Jagle, "parse groups failed, ", e);
                }
                if (callback != null){
                    callback.onFinish();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                JLog.e(Developer.Jagle, "load groups failed, ", error);
                if (callback != null){
                    callback.onFinish();
                }
            }
        });
    }


    public boolean checkUrlInCache(String url) {
        Tables.JsonInfo info = SProperties.pJsonInfo.getValue(url);
        if (info == null) {
            return false;
        }

        if (info.updatetime == null) {
            return false;
        }

        try {
            if (JTimeUtils.diffNowInDays(info.updatetime) < info.liefCycle) {
                JLog.i(Developer.Jagle, "json data in local, update time:" + info.updatetime + "url, " + url);
                return true;
            }
        } catch (ParseException e) {
            return false;
        }
        return false;
    }

    ;

    public synchronized SProperties.MMPNListProperty getGroup(String groupId) {
        SProperties.MMPNListProperty group = SProperties.pMMPNListMap.get(groupId);
        if (group == null) {
            group = new SProperties.MMPNListProperty(groupId);
            SProperties.pMMPNListMap.put(groupId, group);
        }
        return group;
    }

    private void downloadGroupInDbThread(final String groupId, final AsyncCallback callback){
        JLog.d(Developer.Jagle, "downlad mmpn for group :" + groupId);
        AsyncHttpClient client = new AsyncHttpClient();
        final String url = SProperties.pMMGroups.getValue(groupId).groupUrl;
        if (checkUrlInCache(url)) {
            if (callback != null){
                callback.onFinish();
            }
            return;
        }

        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                ObjectMapper mapper = new ObjectMapper();
                mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);
                try {
                    Jsons.MMPNS groups = mapper.readValue(responseBody, Jsons.MMPNS.class);
                    ArrayList<Tables.MMPN> tgroups = new ArrayList<Tables.MMPN>(groups.pns.size());
                    for (Jsons.MMPN group : groups.pns) {
                        tgroups.add(JsonToTable.transMMPN(group));
                    }
                    getGroup(groupId).set(tgroups);
                    SProperties.pJsonInfo.add(JsonToTable.transJsonInfo(url, groups.jsonInfo));
                } catch (IOException e) {
                    JLog.e(Developer.Jagle, "prase url failed " + url + ",", e);
                } finally {
                    if (callback != null) {
                        callback.onFinish();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                JLog.e(Developer.Jagle, "load url failed " + url + ",", error);
                if (callback != null) {
                    callback.onFinish();
                }
            }
        });
    }

    public void downloadGroup(final String groupId,final AsyncCallback callback) {
        getGroup(groupId);
        DataCenter.post(new Runnable() {
            @Override
            public void run() {
                downloadGroupInDbThread(groupId, callback);
            }
        });
    }

    public synchronized  SProperties.DigestProperty getDigest(String mmid) {
        SProperties.DigestProperty digests = SProperties.pDisgestMap.get(mmid);
        if (digests == null) {
            digests = new SProperties.DigestProperty(mmid);
            SProperties.pDisgestMap.put(mmid, digests);
        }
        return digests;
    }

    private PersistentCookieStore getCookie(String mmid){
        synchronized (mCookies) {
            PersistentCookieStore cs = mCookies.get(mmid);
            if (cs == null) {
                cs = new PersistentCookieStore(App.getApp());
                //setCookie(cs, "SUIR", "FC03DE4947436624120D4189471D1965", ".weixin.sogou.com");
                //setCookie(cs, "SUID", "FD376BDA05C60D0A00000000560237D1", ".weixin.sogou.com");
                setCookie(cs, "SNUID", "B81ED3FD8D8BAEDD51A7A37" + Long.toHexString(System.currentTimeMillis()), ".weixin.sogou.com");
            }
            return cs;
        }
    }

    private void downloadDigestsInDbThread(final String mmid, final String openid, final String ext, final AsyncCallback callback) {
        JLog.d(Developer.Jagle, "download digest for mmid:" + mmid + " openid:" + openid);
        AsyncHttpClient client = new AsyncHttpClient();
        client.setURLEncodingEnabled(false);
        RequestParams rp = new RequestParams();
        rp.add("cb", "sogou.weixin.gzhcb");
        rp.add("openid", openid);
        rp.add("ext", ext);
        rp.add("eqs", SogouAes.aes(openid));
        rp.add("ekv", "3");
        rp.add("page", "1");



        client.setCookieStore(getCookie(mmid));

        client.get(URL, rp, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String str = new String(responseBody);
                int begin = str.indexOf("({");
                int end = str.lastIndexOf("})");
                String sub = str.substring(begin + 1, end + 1);
                ObjectMapper mapper = new ObjectMapper();
                mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);
                try {
                    Jsons.MMPNDigests groups = mapper.readValue(sub, Jsons.MMPNDigests.class);
                    ObjectMapper xmlMapper = new XmlMapper();
                    xmlMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
                    ArrayList<Tables.Digest> tdigests = new ArrayList<Tables.Digest>(groups.items.size());
                    for (String item : groups.items) {
                        Xmls.Document value = xmlMapper.readValue(item, Xmls.Document.class);
                        tdigests.add(XmlToTable.transDigest(value.item.display));
                    }
                    getDigest(mmid).set(tdigests);
                } catch (IOException e) {
                    JLog.e(Developer.Jagle, "prase url failed: url %1$s, mmid %2$s, openid %3$s, ext %4$s", URL, mmid, openid, ext);
                    JLog.d(Developer.Jagle, "sub:%s", sub);
                } finally {
                    if (callback != null) {
                        callback.onFinish();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                JLog.e(Developer.Jagle, "get digests failed", error);
                if (callback != null) {
                    callback.onFinish();
                }
            }
        });
    }

    void downloadDigests(final String mmid, final String openid, final String ext, final AsyncCallback callback) {
        getDigest(mmid);
        if(TextUtils.isEmpty(openid)){
            JLog.e(Developer.Jagle, "empty openid for "+ mmid);
            if (callback != null) {
                callback.onFinish();
            }
            return;
        }
        DataCenter.post(new Runnable() {
            @Override
            public void run() {
                downloadDigestsInDbThread(mmid, openid, ext, callback);
            }
        });
    }

    void downloadDigests(final Tables.MMPN mmpn, final AsyncCallback callback){
        downloadDigests(mmpn.mmid, mmpn.openid, mmpn.ext, callback);
    }

    public void storeCookie(String url, String mmid){
        if (TextUtils.isEmpty(mmid)){
            return;
        }

        List<Cookie> cookies = mCookies.get(mmid).getCookies();

        for (Cookie c : cookies) {
            StringBuffer sb = new StringBuffer();
            sb.append(c.getName() + "=" + c.getValue() + "; domain=" + c.getDomain());
            CookieManager.getInstance().setCookie(url, sb.toString());
        }

    }

    private static void setCookie(PersistentCookieStore cs, String key, String value, String domain){
        BasicClientCookie bc = new BasicClientCookie(key, value);
        bc.setDomain(domain);
        bc.setPath("/");
        cs.addCookie(bc);
    }

    public synchronized  ListProperty<Digest> getLastDigest(String groupId) {
        ListProperty<Digest> digests = SProperties.pLastDisgestMap.get(groupId);
        if (digests == null) {
            digests = new  ListProperty<Digest>();
            SProperties.pLastDisgestMap.put(groupId, digests);
        }
        return digests;
    }

    public void updateLastDigests(String groupId){
        JLog.d(Developer.Jagle, "updateLastDigests: " + groupId);
        List<Tables.MMPN> mmpns = getGroup(groupId).get();
        if (mmpns == null || mmpns.size() == 0){
            return;
        }
        ArrayList<Digest> digests = new ArrayList<>();
        for(Tables.MMPN mmpn: mmpns){
            List<Tables.Digest> tds = getDigest(mmpn.mmid).get();
            if (tds == null || tds.size() == 0){
                continue;
            }
            digests.add(Digest.getFromTable(tds.get(0), mmpn));
        }
        getLastDigest(groupId).set(digests);
    }

    public static interface AsyncCallback{
        public void onFinish();
    }

}
