package com.jagle.mmhelper.biz;

import com.jagle.kit.utils.JTimeUtils;
import com.jagle.mmhelper.data.Jsons;
import com.jagle.mmhelper.data.Tables;

/**
 * Created by jagle on 15/8/18.
 */
public class JsonToTable {
    public static Tables.MMGroup transMMGroup(Jsons.MMGroup group){
        Tables.MMGroup t = new Tables.MMGroup();
        t.typeId = group.typeId;
        t.imageUrl = group.imageUrl;
        t.label = group.label;
        t.groupUrl = group.groupUrl;
        return t;
    }

    public static Tables.JsonInfo transJsonInfo(String url, Jsons.JsonInfo info){
        Tables.JsonInfo t = new Tables.JsonInfo();
        t.jsonName = info.jsonName;
        t.liefCycle = info.liefCycle;
        t.timestamp = info.timestamp;
        t.version = info.version;
        t.jsonUrl = url;
        t.updatetime = JTimeUtils.currentTime();
        return t;
    }

    public static Tables.MMPN transMMPN(Jsons.MMPN mmpn){
        Tables.MMPN t = new Tables.MMPN();
        t.orderId = mmpn.orderId;
        t.label = mmpn.label;
        t.description = mmpn.description;
        t.imageUrl = mmpn.imageUrl;
        t.mmid = mmpn.mmid;
        t.webUrl = mmpn.webUrl;
        t.weiboUrl = mmpn.weiboUrl;
        t.openid = mmpn.openid;
        t.detailUrl = mmpn.detailUrl;
        t.ext = mmpn.ext;
        return t;
    }

}
