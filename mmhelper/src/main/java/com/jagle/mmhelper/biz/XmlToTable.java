package com.jagle.mmhelper.biz;

import com.jagle.mmhelper.data.Tables;
import com.jagle.mmhelper.data.Xmls;

/**
 * Created by jagle on 15/9/11.
 */
public class XmlToTable {
    public static Tables.Digest transDigest(Xmls.Display display){
        Tables.Digest result = new Tables.Digest();
        result.docid = display.docid;
        result.tplid = display.tplid;
        result.title = display.title;
        result.title1 = display.title1;
        result.headimage = display.headimage;
        result.sourcename = display.sourcename;
        result.imglink = display.imglink;
        result.content168 = display.content168;
        result.site = display.site;
        result.isV = display.isV;
        result.openid = display.openid;
        result.content = display.content;
        result.showurl = display.showurl;
        result.date = display.date;
        result.url = display.url;
        return result;
    }
}
