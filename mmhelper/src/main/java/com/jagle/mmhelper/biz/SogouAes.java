package com.jagle.mmhelper.biz;

import android.util.Log;

import com.jagle.kit.common.Developer;
import com.jagle.kit.common.JLog;

import org.bouncycastle.crypto.BufferedBlockCipher;
import org.bouncycastle.crypto.CryptoException;
import org.bouncycastle.crypto.engines.RijndaelEngine;
import org.bouncycastle.crypto.modes.CBCBlockCipher;
import org.bouncycastle.crypto.paddings.PKCS7Padding;
import org.bouncycastle.crypto.paddings.PaddedBufferedBlockCipher;
import org.bouncycastle.crypto.paddings.ZeroBytePadding;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.params.ParametersWithIV;
import org.bouncycastle.util.encoders.Base64;
import org.bouncycastle.util.encoders.Hex;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * Created by jagle on 15/9/11.
 */

//SogouEncrypt.setKv("8d03ae022be","3");window.aes=SogouEncrypt.encryptquery("oIWsFtxG-2J2sGx3l5-pknZDv60g","sogou")

//message:oIWsFtxG-2J2sGx3l5-pknZDv60ghdq=sogou
//key:8d03ae022besogou
//iv:0000000000000000
public class SogouAes {
    private static String mC;
    private static String mI;

    public static String APID = "oIWsFtxG-2J2sGx3l5-pknZDv60ghdq=sogou";
    private static final String HEXES = "0123456789ABCDEF";
    private static final String ZERO_PADDING_KEY = "8d03ae022besogou";
    private static final String IV = "0000000000000000";

    public static String encryptquery(String a, String b){
        return null;
    }

    public static String aes(String url){
        try {
            Hex hex = new Hex();

            byte[] key = ZERO_PADDING_KEY.getBytes("UTF-8");
            byte[] iv = IV.getBytes("UTF-8");

            PaddedBufferedBlockCipher c = new PaddedBufferedBlockCipher(new CBCBlockCipher(new RijndaelEngine(128)), new PKCS7Padding());
            c.init(true, new ParametersWithIV(new KeyParameter(key), iv));

            byte[] res = cipherData(c, url.getBytes("UTF-8"));
            String resul = new String(res);
            String  b = Base64.toBase64String(res);
            int length = b.length();
            b = addCode(b, "sogou");
            JLog.d(Developer.Jagle, "length" + length);

            return b;
        } catch (Exception e) {
            JLog.e(Developer.Jagle, "ENCRYPT ERROR", e.getMessage());
        }
        return "";
    }

    public String decryptUrl(String encrypted) {
        try {
            Hex hex = new Hex();
            byte[] key = ZERO_PADDING_KEY.getBytes("UTF-8");
            byte[] iv = IV.getBytes("UTF-8");


            PaddedBufferedBlockCipher c = new PaddedBufferedBlockCipher(new CBCBlockCipher(new RijndaelEngine(128)), new PKCS7Padding());
            c.init(false, new ParametersWithIV(new KeyParameter(key), iv));

            byte[] decryptedText = cipherData(c, (byte[]) hex.decode(encrypted));
            String decrypted = new String(decryptedText, "UTF-8");
            Log.d("DECRYPTED", decrypted);

            return decrypted;
        } catch (Exception e) {
            try {
                throw new CryptoException("Unable to decrypt", e);
            } catch (CryptoException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        }
        return "";
    }

    private static byte[] cipherData(PaddedBufferedBlockCipher cipher, byte[] data) throws Exception {

        int minSize = cipher.getOutputSize(data.length);
        byte[] outBuf = new byte[minSize * 2];
        int length1 = cipher.processBytes(data, 0, data.length, outBuf, 0);
        int length2 = cipher.doFinal(outBuf, length1);
        int actualLength = length1 + length2;
        byte[] result = new byte[actualLength];
        System.arraycopy(outBuf, 0, result, 0, result.length);
        return result;
    }

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEXES.charAt(v >>> 4);
            hexChars[j * 2 + 1] = HEXES.charAt(v & 0x0F);
        }
        return new String(hexChars);
    }

    public void setKv(String c, String i){
        mC = c;
        mI = i;
    }

    public static String addCode(String in, String code){
        int i = 0;
        StringBuffer sb = new StringBuffer();
        for(int j = 0; j<in.length(); j++){
            sb.append(in.charAt(j));
            if (i< code.length() && j == Math.pow(2, i)){
                sb.append(code.charAt(i++));
            }
        }
        return sb.toString();
    }

}
