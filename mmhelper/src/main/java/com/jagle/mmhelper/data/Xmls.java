package com.jagle.mmhelper.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlCData;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;

/**
 * Created by jagle on 15/9/9.
 */
public class Xmls {

    @JacksonXmlRootElement(localName = "DOCUMENT")
    public static class Document {
//        @JacksonXmlCData(value = true)
//        private String docid;

        @JacksonXmlProperty(localName = "item")
        public Item item;

        // getters, setters, toString, etc
    }

    public static class Item {

        @JacksonXmlCData
        public String key;

        @JacksonXmlCData
        public String tplid;

        public String classid;

        @JacksonXmlProperty(localName = "display")
        public Display display;
    }

    public static class Display {
        public String docid;

        public String tplid;

        @JacksonXmlCData
        public String title;

        @JacksonXmlCData
        public String url;

        @JacksonXmlCData
        public String title1;

        @JacksonXmlCData
        public String imglink;

        @JacksonXmlCData
        public String headimage;

        @JacksonXmlCData
        public String sourcename;

        @JacksonXmlCData
        public String content168;

        @JacksonXmlCData
        public String site;

        @JacksonXmlCData
        public String isV;

        @JacksonXmlCData
        public String openid;

        @JacksonXmlCData
        public String content;

        @JacksonXmlCData
        public String showurl;

        @JacksonXmlCData
        public String date;

    }


}
