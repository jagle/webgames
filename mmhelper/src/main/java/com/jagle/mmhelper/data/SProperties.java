package com.jagle.mmhelper.data;

import com.jagle.kit.property.ListProperty;
import com.jagle.threesdk.DBListProperty;

import java.util.HashMap;

/**
 * Created by jagle on 15/8/17.
 */
public class SProperties {

    public final static ListProperty<MMPublic> mMMPublics = new ListProperty<>();

    public final static DBListProperty<Tables.JsonInfo, String> pJsonInfo =
            new DBListProperty<Tables.JsonInfo, String>(Tables.JsonInfo.class) {
                @Override
                public String getKey(Tables.JsonInfo data) {
                    return data.jsonUrl;
                }
            };

    public final static DBListProperty<Tables.MMGroup, String> pMMGroups =
            new DBListProperty<Tables.MMGroup, String>(Tables.MMGroup.class) {
                @Override
                public String getKey(Tables.MMGroup data) {
                    return data.typeId;
                }
            };

    public final static HashMap<String, MMPNListProperty> pMMPNListMap = new HashMap<>();

    public final static HashMap<String, DigestProperty> pDisgestMap = new HashMap<>();

    public final static HashMap<String, ListProperty<Digest>> pLastDisgestMap = new HashMap<>();

    public static final class MMPNListProperty extends  DBListProperty<Tables.MMPN, String>{

        public MMPNListProperty(String tableName) {

            super(Tables.MMPN.class, tableName);
        }

        @Override
        public String getKey(Tables.MMPN data) {
            return data.orderId;
        }
    }

    public static final class DigestProperty extends  DBListProperty<Tables.Digest, Integer>{

        public DigestProperty( String tableName) {

            super(Tables.Digest.class, tableName);
        }

        @Override
        public Integer getKey(Tables.Digest data) {
            return data.id;
        }
    }

    public static void init(){
    }

}
