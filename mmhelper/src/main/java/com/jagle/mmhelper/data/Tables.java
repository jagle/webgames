package com.jagle.mmhelper.data;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlCData;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.jagle.threesdk.JDaoImpl;

/**
 * Created by jagle on 15/8/18.
 */
public class Tables {

    @DatabaseTable(tableName = "groups")
    public static class MMGroup {
        @DatabaseField(id = true, canBeNull = false)
        public String typeId;
        @DatabaseField
        public String imageUrl;
        @DatabaseField
        public String groupUrl;
        @DatabaseField
        public String label;
    }

    @DatabaseTable(tableName = "json_info")
    public static class JsonInfo{
        @DatabaseField(id = true, canBeNull = false)
        public String jsonUrl;
        @DatabaseField
        public String jsonName;
        @DatabaseField
        public int version;
        @DatabaseField
        public String timestamp;
        @DatabaseField
        public int liefCycle;
        @DatabaseField
        public String updatetime;
    }

    @DatabaseTable(daoClass=JDaoImpl.class)
    public static class MMPN {
        @DatabaseField(id = true, canBeNull = false)
        public String orderId;
        @DatabaseField
        public String mmid;
        @DatabaseField
        public String imageUrl;
        @DatabaseField
        public String label;
        @DatabaseField
        public String description;
        @DatabaseField
        public String detailUrl;
        @DatabaseField
        public String webUrl;
        @DatabaseField
        public String weiboUrl;
        @DatabaseField
        public String openid;
        @DatabaseField
        public String ext;
    }

    @DatabaseTable(daoClass=JDaoImpl.class)
    public static class Digest{
        @DatabaseField(generatedId = true)
        public int id;
        @DatabaseField
        public String docid;
        @DatabaseField
        public String tplid;
        @DatabaseField
        public String title;
        @DatabaseField
        public String url;
        @DatabaseField
        public String title1;
        @DatabaseField
        public String imglink;
        @DatabaseField
        public String headimage;
        @DatabaseField
        public String sourcename;
        @DatabaseField
        public String content168;
        @DatabaseField
        public String site;
        @DatabaseField
        public String isV;
        @DatabaseField
        public String openid;
        @DatabaseField
        public String content;
        @DatabaseField
        public String showurl;
        @DatabaseField
        public String date;

    }


}
