package com.jagle.mmhelper.data;

/**
 * Created by jagle on 15/9/17.
 */
public class Digest {
    public String title;

    public String imageUrl;

    public String content;

    public String date;

    public String url;

    public String orderid;

    public String mmlable;

    public static Digest getFromTable(Tables.Digest digest, Tables.MMPN mmpn){
        Digest d = new Digest();
        d.title = digest.title1;
        d.content = digest.content168;
        d.url = digest.url;
        d.imageUrl = digest.imglink;
        d.date = digest.date;
        d.orderid = mmpn.orderId;
        d.mmlable = mmpn.label;
        return d;
    }
}
