package com.jagle.mmhelper.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;

/**
 * Created by jagle on 15/8/17.
 */
public class Jsons {

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class JsonInfo{
        public String jsonName;
        public int version;
        public String timestamp;
        public int liefCycle;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class MMGroups{
        public JsonInfo jsonInfo;
        public ArrayList<MMGroup> groups;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class MMGroup{
        public String typeId;
        public String imageUrl;
        public String groupUrl;
        public String label;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class MMPNS{
        public JsonInfo jsonInfo;
        public ArrayList<MMPN> pns;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class MMPN{
        public String orderId;
        public String mmid;
        public String imageUrl;
        public String label;
        public String description;
        public String detailUrl;
        public String webUrl;
        public String weiboUrl;
        public String openid;
        public String ext;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class MMPNDigests{
        public int totalItems;
        public int totalPages;
        public int page;
        public ArrayList<String> items;
    }
}
