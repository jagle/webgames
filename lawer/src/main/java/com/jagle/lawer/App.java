package com.jagle.lawer;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.jagle.lawer.activities.FeedbackActivity;
import com.jagle.lawer.biz.OrmliteHelper;
import com.jagle.lawer.biz.Property;
import com.jagle.kit.base.JApplication;
import com.jagle.threesdk.AsyncImageView;
import com.umeng.fb.push.FeedbackPush;
import com.umeng.message.PushAgent;

/**
 * Created by jagle on 15/8/17.
 */
public class App extends JApplication{

    private PushAgent mPushAgent;

    @Override
    public void onCreate() {
        super.onCreate();

        mPushAgent = PushAgent.getInstance(this);
        mPushAgent.setDebugMode(BuildConfig.DEBUG);
    }

    @Override
    public void onAppCreate(String processName) {
        super.onAppCreate(processName);
        OpenHelperManager.getHelper(this, OrmliteHelper.class);
        AsyncImageView.initConfig(this);
        Property.getInstance().downloadGroups();
        FeedbackPush.getInstance(this).init(FeedbackActivity.class, true);
    }

    @Override
    public boolean isDebug() {
        return BuildConfig.DEBUG;
    }
}
