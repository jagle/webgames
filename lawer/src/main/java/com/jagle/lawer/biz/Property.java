package com.jagle.lawer.biz;

import android.text.TextUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jagle.kit.common.JToast;
import com.jagle.lawer.App;
import com.jagle.lawer.data.Jsons;
import com.jagle.lawer.data.Properties;
import com.jagle.lawer.data.Tables;
import com.jagle.kit.common.Developer;
import com.jagle.kit.common.JLog;
import com.jagle.kit.property.ValueProperty;
import com.jagle.kit.utils.JFileUtils;
import com.jagle.kit.utils.JTimeUtils;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

/**
 * Created by jagle on 15/10/28.
 */
public class Property {
    public static final String URL = "http://7xnvug.com1.z0.glb.clouddn.com";

    private static Property mInstance;

    private Property() {

    }

    public static Property getInstance() {
        if (mInstance == null) {
            mInstance = new Property();
        }

        return mInstance;
    }

    public void downloadGroups(){
        JLog.d(Developer.Jagle, "downloadGroup");
        final String groupUrl = URL + "/jsons/groups.json";
        if (checkUrlInCache(groupUrl)) {
            return;
        }

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(groupUrl, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                ObjectMapper mapper = new ObjectMapper();
                try {
                    Jsons.LawGroupJson json = mapper.readValue(responseBody, Jsons.LawGroupJson.class);
                    ArrayList<Tables.LawGroup> tgroups = new ArrayList<Tables.LawGroup>(json.groups.size());
                    for (Jsons.LawGroup group : json.groups) {
                        tgroups.add(JsonToTable.translate(group));
                    }
                    Properties.pGroups.set(tgroups);
                    Properties.pJsonInfo.add(JsonToTable.translate(json.jsonInfo, groupUrl));
                } catch (IOException e) {
                    JLog.e(Developer.Jagle, "parse groups failed, ", e);
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                JLog.e(Developer.Jagle, "load groups failed, ", error);
                JToast.show("网络异常");
            }
        });
    }

    public void downloadLawList(final String groupId) {
        Tables.LawGroup group = Properties.pGroups.getValue(groupId);
        if (group == null){
            return;
        }

        final String path = group.path;
        JLog.d(Developer.Jagle, "downloadGroup");
        final String contractUrl = URL + "/jsons/"+ path + ".json";
        if (checkUrlInCache(contractUrl)) {
            return;
        }
        JLog.i(Developer.Jagle, "download law list:" + contractUrl);

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(contractUrl, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody) {
                if (path.equals("ssf")){
                    JLog.i(Developer.Jagle, "test");
                }
                ObjectMapper mapper = new ObjectMapper();
                try {
                    Jsons.ContractJson json = mapper.readValue(responseBody, Jsons.ContractJson.class);
                    ArrayList<Tables.Law> tList = new ArrayList<Tables.Law>(json.laws.size());
                    for (Jsons.Law group : json.laws) {
                        tList.add(JsonToTable.translate(group));
                    }
                    getLawList(groupId).set(tList);
                    Properties.pJsonInfo.add(JsonToTable.translate(json.jsonInfo, contractUrl));
                } catch (IOException e) {
                    JLog.e(Developer.Jagle, "parse groups failed, ", e);
                }
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, byte[] responseBody, Throwable error) {
                JLog.e(Developer.Jagle, "load groups failed, " + contractUrl, error);
                JToast.show("网络异常");
            }
        });
    }

    public boolean checkUrlInCache(String url) {
        Tables.JsonInfo info = Properties.pJsonInfo.getValue(url);
        if (info == null) {
            return false;
        }

        if (info.updatetime == null) {
            return false;
        }

        try {
            if (JTimeUtils.diffNowInDays(info.updatetime) < info.liefCycle) {
                JLog.i(Developer.Jagle, "json data in local, update time:" + info.updatetime + "url, " + url);
                return true;
            }
        } catch (ParseException e) {
            return false;
        }
        return false;
    }

    public void readContractFromDisk(String path, JFileUtils.TextReadCallback callback) {
        File dir = App.getApp().getExternalFilesDir("laws");
        String filePath = dir.getPath() + "/" + path;
        JFileUtils.asyncReadTextFile(filePath, callback);
    }

    public void writeContractToDisk(String path, String text) {
        File dir = App.getApp().getExternalFilesDir("laws");
        String filePath = dir.getPath() + "/" + path;
        JFileUtils.asyncWriteFile(filePath, text);
    }

    public ValueProperty<String> getContract(String path) {
        synchronized (Properties.pContractCache) {
            ValueProperty<String> data = Properties.pContractCache.get(path);
            if (data == null) {
                data = new ValueProperty<>("");
                Properties.pContractCache.put(path, data);
            }
            return data;
        }
    }

    public void loadContract(final String group, final String path) {

        if (!TextUtils.isEmpty(getContract(path).get())) {
            return;
        }

        readContractFromDisk(path, new JFileUtils.TextReadCallback() {
            @Override
            public void onFinish(String text) {
                if (!TextUtils.isEmpty(text)) {
                    getContract(path).set(text);
                    return;
                } else {
                    loadContractFromNet(group, path);
                }
            }
        });


    }

    private void loadContractFromNet(final String group, final String path) {
        Tables.LawGroup lg = Properties.pGroups.getValue(group);
        if (lg == null){
            return;
        }

        String groupid = lg.path;
        AsyncHttpClient client = new AsyncHttpClient();
        final String url = URL + "/" + groupid +"/" + path;
        client.get(url, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                JLog.d(Developer.Jagle, new String(responseBody));
                String text = new String(responseBody);
                getContract(path).set(text);
                writeContractToDisk(path, text);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                JLog.e(Developer.Jagle, "load contract failed, " + url, error);
                JToast.show("网络异常");
            }
        });
    }

    public Properties.LawProperty getLawList(String groupId){

        synchronized (Properties.pLawListMap) {
            Properties.LawProperty data = Properties.pLawListMap.get(groupId);
            if (data == null) {
                data = new Properties.LawProperty(groupId);
                Properties.pLawListMap.put(groupId, data);
            }
            return data;
        }
    }


}
