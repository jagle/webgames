package com.jagle.lawer.biz;

import com.jagle.lawer.data.Jsons;
import com.jagle.lawer.data.Tables;
import com.jagle.kit.utils.JTimeUtils;

/**
 * Created by jagle on 15/10/28.
 */
public class JsonToTable {
    public static Tables.Law translate(Jsons.Law json){
        Tables.Law table = new Tables.Law();
        table.name = json.name;
        table.path = json.path;
        return table;
    }

    public static Tables.LawGroup translate(Jsons.LawGroup json){
        Tables.LawGroup table = new Tables.LawGroup();
        table.name = json.name;
        table.path = json.path;
        table.groupId = json.groupId;
        return table;
    }

    public static Tables.JsonInfo translate(Jsons.JsonInfo json, String url){
        Tables.JsonInfo table = new Tables.JsonInfo();
        table.url = url;
        table.name = json.name;
        table.version = json.version;
        table.timestamp = json.timestamp;
        table.liefCycle = json.liefCycle;
        table.updatetime = JTimeUtils.currentTime();
        return table;
    }
}
