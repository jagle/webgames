package com.jagle.lawer.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.jagle.kit.base.JUiActivity;
import com.jagle.kit.common.JToast;
import com.jagle.kit.common.ViewRef;
import com.jagle.kit.utils.JAppUtils;
import com.jagle.lawer.R;
import com.umeng.update.UmengUpdateAgent;
import com.umeng.update.UmengUpdateListener;
import com.umeng.update.UpdateResponse;

/**
 * Created by jagle on 15/8/28.
 */
public class AboutUsActiviy extends JUiActivity {
    ViewRef<TextView> mVersionText = new ViewRef<>(this, R.id.abu_cur_version);

    @Override
    public int getContentViewId() {
        return R.layout.activity_about_us;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onCreateContentView() {
        super.onCreateContentView();

        String format = getString(R.string.abu_current_version);
        mVersionText.get().setText(String.format(format, JAppUtils.getVersion(this)));
    }

    public void onUpdate(View v){
        UmengUpdateAgent.setUpdateListener(new UmengUpdateListener() {
            @Override
            public void onUpdateReturned(int i, UpdateResponse updateResponse) {
                if (!updateResponse.hasUpdate){
                    JToast.show("已经是最新版本");
                }
            }
        });
        UmengUpdateAgent.forceUpdate(this);
    }

    public void onFeedback(View v){
        FeedbackActivity.feedback(this);
    }
}
