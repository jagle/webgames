package com.jagle.lawer.activities;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.jagle.kit.base.JListAdapterEx;
import com.jagle.kit.base.JUIFragmentV4;
import com.jagle.kit.property.IValuePropertyListener;
import com.jagle.lawer.R;
import com.jagle.lawer.biz.Property;
import com.jagle.lawer.data.Properties;
import com.jagle.lawer.data.Tables;
import com.jagle.kit.base.JListAdapter;
import com.jagle.kit.common.ViewRef;
import com.jagle.kit.property.Binding;

public class LawListFragment extends JUIFragmentV4 {

    private DigestListAdapter mAdapter;
    private ViewRef<ListView> mList;
    public static  final String DATA_GROUP_ID = "data.group.id";
    private String mGroupId;

    @Override
    protected void onCreateContentView(View view) {
        mList = new ViewRef<ListView>(view, R.id.am_list);
        mList.get().setAdapter(mAdapter);
        mList.get().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Tables.Law contract = mAdapter.getDatas().get(i);
                ContractAcitvity.show(getActivity(),mGroupId, contract.path, contract.name);
            }
        });
    }

    public static Bundle buildArguments(String groupId){
        Bundle b = new Bundle();
        b.putString(DATA_GROUP_ID, groupId);
        return  b;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAdapter = new DigestListAdapter(getActivity());
        mGroupId = getArguments().getString(DATA_GROUP_ID);
        Property.getInstance().downloadLawList(mGroupId);
    }

    @Override
    public int getContentViewId() {
        return R.layout.activity_law_list;
    }


    @Override
    public void onResume() {
        super.onResume();

        Binding.bind(Property.getInstance().getLawList(mGroupId), mAdapter);
        Binding.bind(Properties.pQueryText, mAdapter);
    }

    @Override
    public void onPause() {
        super.onPause();
        Binding.unbind(Property.getInstance().getLawList(mGroupId), mAdapter);
        Binding.unbind(Properties.pQueryText, mAdapter);
    }

    private static class DigestListAdapter extends JListAdapterEx<Tables.Law> implements IValuePropertyListener<String> {
        public DigestListAdapter(Context context){
            super(context, R.layout.item_contract_list);
        }

        @Override
        protected void onItemCreated(final View convertView) {
            super.onItemCreated(convertView);
        }

        @Override
        public void updateView(View view, Tables.Law data) {
            ((TextView)view).setText(data.name);
        }

        @Override
        public void onDataChanged(String data) {
            filter(data);
        }

        @Override
        public String getDataString(Tables.Law data) {
            return data.name;
        }
    }
}
