package com.jagle.lawer.activities;


import android.app.Activity;
import android.os.Bundle;

import com.jagle.lawer.R;
import com.jagle.kit.base.JUIActivityV4;
import com.jagle.kit.utils.JActivityUtils;
import com.umeng.fb.FeedbackAgent;
import com.umeng.fb.fragment.FeedbackFragment;

/**
 * Created by jagle on 15/8/28.
 */
public class FeedbackActivity extends JUIActivityV4 {
    private FeedbackFragment mFeedbackFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            String conversation_id = getIntent().getStringExtra(FeedbackFragment.BUNDLE_KEY_CONVERSATION_ID);
            mFeedbackFragment = FeedbackFragment.newInstance(conversation_id);

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container,mFeedbackFragment)
                    .commit();
        }
    }

    @Override
    public int getContentViewId() {
        return R.layout.activity_feedback;
    }

    @Override
    protected void onNewIntent(android.content.Intent intent) {
        mFeedbackFragment.refresh();
    }


    public static void feedback(Activity act){
        Bundle b = new Bundle();
        String id = new FeedbackAgent(act).getDefaultConversation().getId();
        b.putString(FeedbackFragment.BUNDLE_KEY_CONVERSATION_ID, id);
        JActivityUtils.jump(act, FeedbackActivity.class, b);
    }
}
