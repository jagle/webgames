package com.jagle.lawer.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.jagle.lawer.R;
import com.jagle.lawer.biz.Property;
import com.jagle.kit.base.JUiActivity;
import com.jagle.kit.base.JWebView;
import com.jagle.kit.common.ViewRef;
import com.jagle.kit.property.Binding;
import com.jagle.kit.utils.JActivityUtils;

/**
 * Created by jagle on 15/10/28.
 */
public class ContractAcitvity extends JUiActivity {
    public static final String DATA_PATH = "data.path";
    public static final String DATA_GROUP = "data.group";
    private String mGroup;
    private String mPath;

    ViewRef<JWebView> mWebView = new ViewRef<JWebView>(this, R.id.ac_text);
    @Override
    public int getContentViewId() {
        return R.layout.activity_contract;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPath = getIntent().getStringExtra(DATA_PATH);
        mGroup = getIntent().getStringExtra(DATA_GROUP);
        Property.getInstance().loadContract(mGroup, mPath);

        mWebView.get().setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return true;
            }

            @Override
            public boolean shouldOverrideKeyEvent(WebView view, KeyEvent event) {
                return true;
            }
        });

        mWebView.get().setFocusableInTouchMode(false);
        mWebView.get().setFocusable(false);
        mWebView.get().setClickable(false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Binding.bind(Property.getInstance().getContract(mPath), mWebView.get());
    }

    @Override
    protected void onPause() {
        super.onPause();
        Binding.unbind(Property.getInstance().getContract(mPath), mWebView.get());
    }

    @Override
    protected void onCreateContentView() {
        super.onCreateContentView();
        WebSettings settings = mWebView.get().getSettings();
        settings.setLoadWithOverviewMode(true);
        settings.setSupportZoom(true);
        settings.setBuiltInZoomControls(true);
        settings.setDisplayZoomControls(false);
    }

    public static void show(Activity act,String group, String path, String title){
        Bundle b = new Bundle();
        b.putString(DATA_GROUP, group);
        b.putString(DATA_PATH, path);
        b.putString(JUiActivity.DATA_ACTIVITY_TITLE, title);
        JActivityUtils.jump(act, ContractAcitvity.class, b);
    }

}
