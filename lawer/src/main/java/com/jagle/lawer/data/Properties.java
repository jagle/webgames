package com.jagle.lawer.data;

import android.util.LruCache;

import com.jagle.kit.property.ValueProperty;
import com.jagle.threesdk.DBListProperty;

import java.util.HashMap;

/**
 * Created by jagle on 15/10/28.
 */
public class Properties {
    public final static DBListProperty<Tables.JsonInfo, String> pJsonInfo =
            new DBListProperty<Tables.JsonInfo, String>(Tables.JsonInfo.class) {
                @Override
                public String getKey(Tables.JsonInfo data) {
                    return data.url;
                }
            };

    public final static class LawProperty extends DBListProperty<Tables.Law, String>{
        public LawProperty(String tableName) {
            super(Tables.Law.class, tableName);
        }

        @Override
        public String getKey(Tables.Law data) {
            return data.name;
        }
    };

    public final static DBListProperty<Tables.LawGroup, String> pGroups = new DBListProperty<Tables.LawGroup, String>(Tables.LawGroup.class) {
        @Override
        public String getKey(Tables.LawGroup data) {
            return data.groupId;
        }
    };

    public final static HashMap<String, LawProperty> pLawListMap = new HashMap<>();

    public final static LruCache<String, ValueProperty<String>> pContractCache = new LruCache<>(4 * 1024 * 1024);

    public final static ValueProperty<String> pQueryText = new ValueProperty<>("");
}
