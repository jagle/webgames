package com.jagle.lawer.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;

/**
 * Created by jagle on 15/10/28.
 */
public class Jsons {
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class JsonInfo{
        public String name;
        public int version;
        public String timestamp;
        public int liefCycle;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Law {
        public String name;
        public String path;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class ContractJson{
        public JsonInfo jsonInfo;
        public ArrayList<Law> laws;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class LawGroup{
        public String groupId;
        public String name;
        public String path;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class LawGroupJson{
        public JsonInfo jsonInfo;
        public ArrayList<LawGroup> groups;
    }

}
