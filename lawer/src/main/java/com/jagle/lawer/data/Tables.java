package com.jagle.lawer.data;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.jagle.threesdk.JDaoImpl;

/**
 * Created by jagle on 15/10/28.
 */
public class Tables {
    @DatabaseTable(tableName = "json_info")
    public static class JsonInfo{
        @DatabaseField(id = true, canBeNull = false)
        public String url;
        @DatabaseField
        public String name;
        @DatabaseField
        public int version;
        @DatabaseField
        public String timestamp;
        @DatabaseField
        public int liefCycle;
        @DatabaseField
        public String updatetime;
    }

    @DatabaseTable(daoClass=JDaoImpl.class)
    public static class Law {
        @DatabaseField(id = true, canBeNull = false)
        public String name;
        @DatabaseField
        public String path;
    }

    @DatabaseTable(tableName = "law_groups")
    public static class LawGroup{
        @DatabaseField(id = true, canBeNull = false)
        public String groupId;
        @DatabaseField
        public String name;
        @DatabaseField
        public String path;
    }




}
