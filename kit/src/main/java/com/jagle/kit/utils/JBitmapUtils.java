package com.jagle.kit.utils;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class JBitmapUtils {
    public static final String TYPE_PNG = "png";
    public static final String TYPE_JPG = "jpg";
    public static final String TYPE_JPEG = "jpeg";

    public static final int ARGB_8888 = 1;
    public static final int RGB_565 = 2;

    public static boolean saveToFile(Bitmap bitmap, String filePath) {
        return saveToFile(bitmap, JFileUtils.getPrarentPath(filePath), JFileUtils.getName(filePath));
    }

    public static boolean saveToFile(Bitmap bitmap, String dirPath, String fileName, CompressFormat format) {
        File dir = new File(dirPath);
        if (!dir.exists()) {
            dir.mkdirs();
        }

        File file = new File(dir, fileName);
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(file);
            bitmap.compress(format, 85, out);
            out.flush();
            out.close();
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            try {
                out.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        return false;
    }

    public static boolean saveToFile(Bitmap bitmap, String dirPath, String fileName) {
        String ex = JFileUtils.getExtension(fileName);
        CompressFormat format = CompressFormat.PNG;
        if (ex.equalsIgnoreCase(TYPE_PNG)) {
            format = CompressFormat.PNG;
        } else if (ex.equalsIgnoreCase(TYPE_JPG) || ex.equalsIgnoreCase(TYPE_JPEG)) {
            format = CompressFormat.JPEG;
        }
        return saveToFile(bitmap, dirPath, fileName, format);
    }

    public static int convertByteToInt(byte data) {
        return (int) (0x00ff & data);
    }

    public static int convertArgbBytesToInt(byte[] data) {
        return (int) data[0] + (int) data[1] << 4 + (int) data[2] << 8 + (int) data[3] << 12;
    }

    public static int convertArgbBytesToInt(byte red, byte grenn, byte blue, byte alpha) {
        return (int) blue + (int) grenn << 4 + (int) red << 8 + (int) alpha << 12;
    }

    // 将纯RGB数据数组转化成int像素数组
    public static int[] convertByteArrayToIntArray(byte[] data, int format) {
        int size = data.length;
        if (size == 0) {
            return null;
        }

        int arg = 0;
        if (size % 3 != 0) {
            arg = 1;
        }

        // 一般情况下data数组的长度应该是3的倍数，这里做个兼容，多余的RGB数据用黑色0XFF000000填充
        int[] color = new int[size / 3 + arg];
        int red, green, blue;

        if (arg == 0) {
            for (int i = 0; i < color.length; ++i) {
                red = convertByteToInt(data[i * 3]);
                green = convertByteToInt(data[i * 3 + 1]);
                blue = convertByteToInt(data[i * 3 + 2]);

                // 获取RGB分量值通过按位或生成int的像素值
                color[i] = (red << 16) | (green << 8) | blue | 0xFF000000;
            }
        } else {
            for (int i = 0; i < color.length - 1; ++i) {
                red = convertByteToInt(data[i * 3]);
                green = convertByteToInt(data[i * 3 + 1]);
                blue = convertByteToInt(data[i * 3 + 2]);
                color[i] = (red << 16) | (green << 8) | blue | 0xFF000000;
            }

            color[color.length - 1] = 0xFF000000;
        }

        return color;
    }

}
