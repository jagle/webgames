package com.jagle.kit.utils;

import android.content.Context;

public class JDimensUtil {

    public static int dip2px(Context context, float dpValue) {
        final float scale = JDeviceUtil.getDensity(context);
        return (int) (dpValue * scale + 0.5f);
    }

    public static int px2dip(Context context, float pxValue) {
        final float scale = JDeviceUtil.getDensity(context);
        return (int) (pxValue / scale + 0.5f);
    }

}
