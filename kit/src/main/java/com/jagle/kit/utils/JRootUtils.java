package com.jagle.kit.utils;

import com.jagle.kit.common.Developer;
import com.jagle.kit.common.JLog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class JRootUtils {

    public static boolean findBinary(final String binaryName) {
        boolean found = false;

        final List<String> list = new ArrayList<String>();
        String[] places = {"/sbin/", "/system/bin/", "/system/xbin/", "/data/local/xbin/",
                "/data/local/bin/", "/system/sd/xbin/", "/system/bin/failsafe/", "/data/local/"};

        JLog.i(Developer.Jagle, "Checking for " + binaryName);
        for (String where : places) {
            if (JFileUtils.isExist(where + binaryName) && JFileUtils.canExecute(where + binaryName)) {
                JLog.i(Developer.Jagle, binaryName + " was found here: " + where);
                list.add(where);

                found = true;
            } else {
                JLog.i(Developer.Jagle, binaryName + " was NOT found here: " + where);
            }
        }

        if (!found) {
            JLog.i(Developer.Jagle, "Trying third method");

            try {
                List<String> paths = getPath();

                if (paths != null) {
                    for (String path : paths) {
                        String filePath = path + "/" + binaryName;
                        if (JFileUtils.isExist(filePath) && JFileUtils.canExecute(filePath)) {
                            JLog.i(Developer.Jagle, binaryName + " was found here: " + path);
                            list.add(path);
                            found = true;
                        } else {
                            JLog.i(Developer.Jagle, binaryName + " was NOT found here: " + path);
                        }
                    }
                }
            } catch (Exception e) {
                JLog.i(Developer.Jagle, binaryName + " was not found, more information MAY be available with Debugging on.");
            }
        }
        return found;
    }

    public static List<String> getPath() {
        return Arrays.asList(System.getenv("PATH").split(":"));
    }

}
