package com.jagle.kit.utils;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.text.TextUtils;
import android.util.DisplayMetrics;

import com.jagle.kit.common.AppPreferences;

import java.io.File;
import java.util.UUID;

public class JDeviceUtil {
    public static DisplayMetrics getDispalyMetrics(Context context) {
        return context.getResources().getDisplayMetrics();
    }

    public static float getDensity(Context context) {
        return context.getResources().getDisplayMetrics().density;
    }

    public static boolean externalStorageWritable() {
        return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
    }

    public static File getExternalFileDir(Context context, String type) {
        return context.getExternalFilesDir(type);
    }

    public static File getExternalPictureFile(Context context, String fileName) {
        return new File(getExternalFileDir(context, Environment.DIRECTORY_PICTURES), fileName);
    }

    public static String getIMEI(Context context) {
        WifiManager wm = (WifiManager) context
                .getSystemService(Context.WIFI_SERVICE);
        String mac = wm.getConnectionInfo().getMacAddress();
        String imei = mac;
        if (imei != null && imei.length() > 0)
            return imei;
        else {
            String rndId = AppPreferences.getString(AppPreferences.KEY_UUID, null);
            if (TextUtils.isEmpty(rndId)) {
                rndId = UUID.randomUUID().toString();
                AppPreferences.setValue(AppPreferences.KEY_UUID, rndId);
            }
            return rndId;
        }
    }
}
