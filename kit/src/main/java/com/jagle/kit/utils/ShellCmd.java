package com.jagle.kit.utils;

import com.jagle.kit.BuildConfig;
import com.jagle.kit.common.Developer;
import com.jagle.kit.common.JLog;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Map;

public class ShellCmd {
    public static String getSystemProperty(String propName) {
        String line;
        BufferedReader input = null;
        try {
            Process p = Runtime.getRuntime().exec("getprop " + propName);
            input = new BufferedReader(
                    new InputStreamReader(p.getInputStream()), 1024);
            line = input.readLine();
            input.close();
        } catch (IOException ex) {
            JLog.e(Developer.Jagle, "Unable to read sysprop " + propName, ex);
            return null;
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    JLog.e(Developer.Jagle, "Exception while closing InputStream", e);
                }
            }
        }
        return line;
    }

    public static ArrayList<String> docmd(String cmd) {
        String line;
        ArrayList<String> out = new ArrayList<String>();
        BufferedReader input = null;
        try {
            String[] environment = new String[System.getenv().size()];
            int i = 0;
            for (Map.Entry<String, String> entry : System.getenv().entrySet()) {
                environment[i] = entry.getKey() + "=" + entry.getValue();
                i++;
            }
            Process p = Runtime.getRuntime().exec(cmd, environment);
            input = new BufferedReader(
                    new InputStreamReader(p.getInputStream()), 1024);
            while ((line = input.readLine() ) != null){
                out.add(line);
            }
        } catch (IOException ex) {
            JLog.e(Developer.Jagle, "Unable to do cmd: " + cmd, ex);
            return null;
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    JLog.e(Developer.Jagle, "Exception while closing InputStream", e);
                }
            }
        }
        return out;
    }

    public static int runSuCommand(String cmd) {
        Process process = null;
        DataOutputStream os = null;
        int exitValue = 0;

        try {
            process = Runtime.getRuntime().exec("su");
            os = new DataOutputStream(process.getOutputStream());

            os.writeBytes(cmd + "\n");
            os.writeBytes("exit $?\n");
            os.flush();

            process.waitFor();

            exitValue = process.exitValue();

            if (BuildConfig.DEBUG) {
               JLog.i(Developer.Jagle, "Exit Code: " + exitValue);

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
                StringBuilder log = new StringBuilder();

                String line;
                while ((line = bufferedReader.readLine()) != null) {
                    log.append(line + "\n");
                }

               JLog.d(Developer.Jagle, log.toString());
            }

        } catch (Exception e) {
           JLog.e(Developer.Jagle, "Failed to run command, exception: " + e.getMessage());
            return 0xdeadeeee;
        } finally {
            try {
                if (os != null) {
                    os.close();
                }
                process.destroy();
            } catch (Exception e) {
               JLog.e(Developer.Jagle, "Failed to destory inject process");
            }
        }

        return exitValue;
    }

    public static boolean startSuProcess(String cmd) {
        Process process = null;
        DataOutputStream os = null;

        try {
            process = Runtime.getRuntime().exec("su");
            os = new DataOutputStream(process.getOutputStream());

            os.writeBytes(cmd + " & \n");
            os.writeBytes("exit\n");
            os.flush();
        } catch (Exception e) {
            JLog.e(Developer.Jagle, "Failed to start process, exception: " + e.getMessage());
            return false;
        } finally {
            try {
                if (os != null) {
                    os.close();
                }
            } catch (Exception e) {
                JLog.e(Developer.Jagle, "Failed to close stream");
            }
        }

        return true;
    }

}


