package com.jagle.kit.utils;

import android.content.Context;
import android.os.AsyncTask;

import com.jagle.kit.base.JApplication;
import com.jagle.kit.common.Developer;
import com.jagle.kit.common.JLog;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class JFileUtils {
    public static char sPathSeparator = File.separatorChar;
    public static char sExtensionSeparator = '.';

    public static String getExtension(String filePath) {
        int dot = filePath.lastIndexOf(sExtensionSeparator);
        return filePath.substring(dot + 1);
    }

    public static String getName(String filePath) {
        return (new File(filePath)).getName();
    }

    public static String getNameWithoutExtension(String filePath) {
        int dot = filePath.lastIndexOf(sExtensionSeparator);
        int sep = filePath.lastIndexOf(sPathSeparator);
        return filePath.substring(sep + 1, dot);
    }

    public static String getPrarentPath(String filePath) {
        int sep = filePath.lastIndexOf(sPathSeparator);
        return filePath.substring(0, sep);
    }

    public static boolean isExist(String filePath) {
        return (new File(filePath)).exists();
    }

    public static boolean canExecute(String filePath) {
        return (new File(filePath)).canExecute();
    }

    public static void renameFile(String oldFilePath, String newFilePath) {
        File parentDir = new File(JFileUtils.getPrarentPath(newFilePath));
        parentDir.mkdirs();

        File oldFile = new File(newFilePath);
        File newFile = new File(oldFilePath);
        newFile.renameTo(oldFile);
    }

    public static void makeParentDirsIfNonExist(String filePath) {
        File file = new File(getPrarentPath(filePath));
        if (!file.exists()) {
            file.mkdirs();
        }
    }

    public static void writeFile(String path, String data) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(path));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        } catch (IOException e) {
            JLog.e(Developer.Jagle, "File write failed: " + path, e.toString());
        }
    }

    public static void asyncWriteFile(final String path, final String data){
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                writeFile(path, data);
                return null;
            }
        }.execute();
    }

    public static void asyncWriteFile(final String path, final byte[] data){
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                writeFile(path, data);
                return null;
            }
        }.execute();
    }

    public static void writeFile(String path, byte[] data){
        try {
            FileOutputStream outputStreamWriter = new FileOutputStream(path);
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        } catch (IOException e) {
            JLog.e(Developer.Jagle, "File write failed: " + path, e.toString());
        }
    }

    public static String readTextFile(String path) {

        StringBuffer sb = new StringBuffer();
        try {
            BufferedReader br = new BufferedReader(new FileReader(path));
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
                sb.append('\n');
            }

        } catch (IOException e) {
            JLog.w(Developer.Jagle, "read file failed:" + path, e);
            return null;
        }
        return sb.toString();
    }

    public static byte[] readBinaryFile(String path) {
        File f = new File(path);
        if (!f.exists()) {
            return null;
        }


        byte[] fileData = new byte[(int) f.length()];
        try {
            DataInputStream dis = new DataInputStream(new FileInputStream(f));
            dis.readFully(fileData);
            dis.close();
        } catch (IOException e) {
            JLog.e(Developer.Jagle, "read file failed:" + path, e);
            return null;
        }
        return fileData;
    }

    public static void asyncReadTextFile(final String path, final TextReadCallback callBack) {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                return readTextFile(path);
            }

            @Override
            protected void onPostExecute(String s) {
                callBack.onFinish(s);
            }
        }.execute();
    }

    public static void asyncReadBinaryFile(final String path, final BinaryReadCallback callBack) {
        new AsyncTask<Void, Void, byte[]>() {
            @Override
            protected byte[] doInBackground(Void... params) {
                return readBinaryFile(path);
            }

            @Override
            protected void onPostExecute(byte[] s) {
                callBack.onFinish(s);
            }
        }.execute();
    }

public static interface TextReadCallback {
    public void onFinish(String text);
}

public static interface BinaryReadCallback {
    public void onFinish(byte[] text);
}


}
