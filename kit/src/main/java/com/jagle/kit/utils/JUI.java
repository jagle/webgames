package com.jagle.kit.utils;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ListAdapter;
import android.widget.ListView;

public class JUI {
    public static int MATCH_PARENT = LayoutParams.MATCH_PARENT;
    public static int WRAP_CONCENT = LayoutParams.WRAP_CONTENT;

    public static Handler mHanlder = new Handler(Looper.getMainLooper());

    /// visibility
    public static int getVisibleOrGone(boolean visible) {
        return visible ? View.VISIBLE : View.GONE;
    }

    public static int getVisibleOrHide(boolean visible) {
        return visible ? View.VISIBLE : View.INVISIBLE;
    }

    public static void reverseVisibilityWithGone(View v) {
        if (v.getVisibility() == View.VISIBLE) {
            v.setVisibility(View.GONE);
        } else {
            v.setVisibility(View.VISIBLE);
        }
    }

    public static void reverseVisibilityWithHide(View v) {
        if (v.getVisibility() == View.VISIBLE) {
            v.setVisibility(View.INVISIBLE);
        } else {
            v.setVisibility(View.VISIBLE);
        }
    }

    public static boolean getVisible(View v) {
        return v.getVisibility() == View.VISIBLE;
    }

    public static void setClickListenerForViews(View parent, View.OnClickListener listener, int... ids) {
        for (int id : ids) {
            View v = parent.findViewById(id);
            v.setOnClickListener(listener);
        }
    }

    public static void setClickListenerForViews(Activity activity, View.OnClickListener listener, int... ids) {
        for (int id : ids) {
            View v = activity.findViewById(id);
            v.setOnClickListener(listener);
        }
    }

    public static void setClickListenerForViews(View.OnClickListener listener, View... views) {
        for (View view : views) {
            view.setOnClickListener(listener);
        }
    }

    public static void setDpPadding(View view, int left, int top, int right, int bottom) {
        view.setPadding(JDimensUtil.dip2px(view.getContext(), left),
                JDimensUtil.dip2px(view.getContext(), top),
                JDimensUtil.dip2px(view.getContext(), right),
                JDimensUtil.dip2px(view.getContext(), bottom));
    }

    public static void measureView(View child) {
        LayoutParams p = child.getLayoutParams();
        if (p == null) {
            p = new LayoutParams(LayoutParams.MATCH_PARENT,
                    LayoutParams.WRAP_CONTENT);
        }

        int childWidthSpec = ViewGroup.getChildMeasureSpec(0, 0 + 0, p.width);
        int lpHeight = p.height;
        int childHeightSpec;

        if (lpHeight > 0) {
            childHeightSpec = MeasureSpec.makeMeasureSpec(lpHeight, MeasureSpec.EXACTLY);
        } else {
            childHeightSpec = MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED);
        }
        child.measure(childWidthSpec, childHeightSpec);
    }

    public static void setListViewHeightBasedOnChildrenCount(ListView listView, int showItemCount) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }

        int totalHeight = 0;
        int realCount = Math.min(showItemCount, listAdapter.getCount());
        for (int i = 0; i < realCount; i++) {
            View listItem = listAdapter.getView(i, null, listView);
            measureView(listItem);
            totalHeight += listItem.getMeasuredHeight();
        }

        LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    public static void post(Runnable runnable) {
        mHanlder.post(runnable);
    }

    public static void postDelay(Runnable runnable, long delayMillis) {
        mHanlder.postDelayed(runnable, delayMillis);
    }
}
