package com.jagle.kit.utils;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

/**
 * @author L
 *         simple jump to target activity or web from current activity
 */
public class JActivityUtils {
    // jump to activity , don't finish current activity
    public static void jump(Activity from, Class<?> to) {
        jump(from, to, false);
    }

    // jump to activity , finish current activity if finish is true
    public static void jump(Activity from, Class<?> to, boolean finish) {
        Intent in = new Intent(from, to);
        from.startActivity(in);

        if (finish) {
            from.finish();
        }
    }

    // jump to activity, with intent data
    public static void jump(Activity from, Class<?> to, Bundle b) {
        jump(from, to, b, false);
    }

    public static void jump(Activity from, Class<?> to, Bundle b, boolean finish) {
        Intent in = new Intent(from, to);
        in.replaceExtras(b);
        from.startActivity(in);

        if (finish) {
            from.finish();
        }
    }

    // start activity for result;
    public static void jump(Activity from, Class<?> to, int requestCode) {
        Intent in = new Intent(from, to);
        from.startActivityForResult(in, requestCode);
    }

    public static void jump(View from, Class<?> to, Bundle b) {
        jump(from, to, b, false);
    }

    public static void jump(View from, Class<?> to, Bundle b, boolean finish) {
        jump((Activity) from.getContext(), to, b, finish);
    }

    public static void jump(View from, Class<?> to) {
        jump(from, to, false);
    }

    public static void jump(View from, Class<?> to, boolean finish) {
        jump((Activity) from.getContext(), to, finish);
    }


    public static void jump(View from, Class<?> to, int requestCode) {
        jump((Activity) from.getContext(), to, requestCode);
    }

    /// jump web
    public static void jumpWeb(Activity from, String url) {
        Uri uri = Uri.parse(url);
        jumpWeb(from, uri);
    }

    public static void jumpWeb(Activity from, Uri uri) {
        from.startActivity(new Intent(Intent.ACTION_VIEW, uri));
    }

    public static void jumpWeb(View from, String url) {
        jumpWeb((Activity) from.getContext(), url);
    }

    public static void jumpWeb(View from, Uri uri) {
        jumpWeb((Activity) from.getContext(), uri);
    }

    // jump system action
    public static void jumpSystemAction(Activity from, String action) {
        Intent intent = new Intent(action);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        from.startActivity(intent);
    }

    public static void jumpNetworkSetting(Activity from) {
        jumpSystemAction(from, android.provider.Settings.ACTION_WIRELESS_SETTINGS);
    }

    public static void jumpAppDetailSetting(Activity from, String packageName) {
        Intent intent = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Uri uri = Uri.fromParts("package", packageName, null);
        intent.setData(uri);
        from.startActivity(intent);
    }

    public static void jumpWifiSetting(Activity from) {
        jumpSystemAction(from, android.provider.Settings.ACTION_WIFI_SETTINGS);
    }

    // choose file
    public static void chooseFile(Activity activity, String type, int requestCode, String description) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType(type);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        try {
            activity.startActivityForResult(Intent.createChooser(intent, description), requestCode);
        } catch (android.content.ActivityNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    public static void chooseFile(Activity activity, String type, int requestCode) {
        chooseFile(activity, type, requestCode, "choose a file");
    }
}
