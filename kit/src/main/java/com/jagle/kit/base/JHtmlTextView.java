package com.jagle.kit.base;

import android.content.Context;
import android.text.Html;
import android.util.AttributeSet;
import android.widget.TextView;

import com.jagle.kit.property.IValuePropertyListener;

/**
 * Created by jagle on 15/10/28.
 */
public class JHtmlTextView extends JTextView {
    public JHtmlTextView(Context context) {
        super(context);
    }

    public JHtmlTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public JHtmlTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void onDataChanged(String data) {
        setText(Html.fromHtml(data));
    }
}
