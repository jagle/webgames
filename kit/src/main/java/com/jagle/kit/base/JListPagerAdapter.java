package com.jagle.kit.base;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.View;

import com.jagle.kit.common.Developer;
import com.jagle.kit.common.JLog;
import com.jagle.kit.property.IListPropertyListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jagle on 15/9/17.
 */
public class JListPagerAdapter<T> extends FragmentPagerAdapter implements IListPropertyListener<T>{
    protected List<T> mObjects;
    private  Class<? extends Fragment> mCls;

    // get item by view class
    public JListPagerAdapter(FragmentManager fm, Class<? extends Fragment> cls, List<T> objects) {
        super(fm);
        mCls = cls;
        mObjects = objects;
    }

    public JListPagerAdapter(FragmentManager fm, Class<? extends Fragment> cls) {
        super(fm);
        mCls = cls;
        mObjects = new ArrayList<>();
    }

    public T getData(int position){
        return mObjects.get(position);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        try {
            fragment =  (Fragment)mCls.newInstance();
            Bundle b = getArguments(position);
            if (b != null){
                fragment.setArguments(b);
            }
        } catch (InstantiationException e) {
            JLog.e(Developer.Jagle, "Instantiation fragment error", e);
        } catch (IllegalAccessException e) {
            JLog.e(Developer.Jagle, "create fragment error", e);
        }
        return fragment;
    }

    public Bundle getArguments(int position){
        if (mObjects == null){
            return null;
        }
        return getArguments(mObjects.get(position));
    }

    public Bundle getArguments(T data){
        return null;
    }

    @Override
    public int getCount() {
        if (mObjects == null) {
            return 0;
        }
        return mObjects.size();
    }


    @Override
    public void onDataChanged(List<T> data) {
        if (mObjects != data){
            mObjects = data;
        }
        notifyDataSetChanged();
    }
}
