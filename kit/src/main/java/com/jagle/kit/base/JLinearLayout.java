package com.jagle.kit.base;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.jagle.kit.utils.JDimensUtil;

public abstract class JLinearLayout extends LinearLayout{
	public JLinearLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public JLinearLayout(Context context) {
		super(context);
		init();
	}

	// get content view layout resource
	public abstract int getContentViewId();
	
	// callback when finish content view inflater 
	protected void onCreateContentView(View v) {};
	
	// do some initialize asynchronous
	protected void asyncInit() {
		onCreateContentView(this);
	}
	
	//set padding use dp
	public void setDpPadding(int left, int top, int right, int bottom){
		setPadding(JDimensUtil.dip2px(getContext(), left), 
				JDimensUtil.dip2px(getContext(), top), 
				JDimensUtil.dip2px(getContext(), right), 
				JDimensUtil.dip2px(getContext(), bottom));
	}
	
	// do base initialize
	private void init() {
		createView();
		
		post(new Runnable() {
			@Override
			public void run() {
				asyncInit();
			}
		});
	}

	// inflater resource
	private void createView() {
		LayoutInflater in = LayoutInflater.from(getContext());
		in.inflate(getContentViewId(), this);
	}

}
