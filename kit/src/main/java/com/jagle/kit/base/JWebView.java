package com.jagle.kit.base;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebView;
import android.widget.TextView;

import com.jagle.kit.property.IValuePropertyListener;

/**
 * Created by jagle on 15/10/28.
 */
public class JWebView extends WebView implements IValuePropertyListener<String> {
    public JWebView(Context context) {
        super(context);
    }

    public JWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public JWebView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void onDataChanged(String data) {
        loadDataWithBaseURL(null, data, "text/html", "utf-8", null);
    }
}
