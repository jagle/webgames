package com.jagle.kit.base;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.jagle.kit.property.IValueProperty;
import com.jagle.kit.property.IValuePropertyListener;

/**
 * Created by jagle on 15/10/28.
 */
public class JTextView extends TextView implements IValuePropertyListener<String> {
    public JTextView(Context context) {
        super(context);
    }

    public JTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public JTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void onDataChanged(String data) {
        setText(data);
    }
}
