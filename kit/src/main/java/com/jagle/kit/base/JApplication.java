package com.jagle.kit.base;

import android.app.ActivityManager;
import android.app.Application;
import android.graphics.drawable.Drawable;
import android.os.StrictMode;

import com.jagle.kit.BuildConfig;
import com.jagle.kit.common.JUncaughtExceptionHandler;

import java.lang.Thread.UncaughtExceptionHandler;

public class JApplication extends Application {
	private static JApplication mSelf;
	private boolean mIsApp = true;

	@Override
	public void onCreate() {
		super.onCreate();
		Thread.currentThread().setContextClassLoader(getClass().getClassLoader());
		
		setUncaughtExceptionHandler();
		
		if (BuildConfig.DEBUG){
			StrictMode.enableDefaults();
		}
		
		mSelf = this;
		int pid = android.os.Process.myPid();
		ActivityManager manager = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
		for (ActivityManager.RunningAppProcessInfo processInfo : manager.getRunningAppProcesses()) {
			if (processInfo.pid == pid) {
				if (getPackageName().equals(processInfo.processName)) {
					mIsApp = true;
					onAppCreate(processInfo.processName);
				} else {
					mIsApp = false;
					onServiceCreate(processInfo.processName);
				}
			}
		}
	}

	public void onServiceCreate(String processName) {

	}

	public void onAppCreate(String processName) {

	}

	public boolean isApp(){
		return mIsApp;
	}


	public static JApplication getApp(){
		return mSelf;
	}

	public boolean isDebug(){
		return BuildConfig.DEBUG;
	}
	

	private void setUncaughtExceptionHandler() {
        UncaughtExceptionHandler dueh = Thread.getDefaultUncaughtExceptionHandler();
        JUncaughtExceptionHandler uEH = new JUncaughtExceptionHandler(dueh);
        Thread.setDefaultUncaughtExceptionHandler(uEH);
    }
}
