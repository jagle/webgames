package com.jagle.kit.common;

import android.content.Context;
import android.widget.Toast;

import com.jagle.kit.BuildConfig;
import com.jagle.kit.base.JApplication;
import com.jagle.kit.utils.JUI;

/**
 * Created by jagle on 15/8/24.
 */
public class JToast {
    private static Toast sToast;

    public static void show(final int resId) {
        JUI.post(new Runnable() {
            @Override
            public void run() {
                if (sToast == null) {
                    sToast = Toast.makeText(JApplication.getApp(), resId, Toast.LENGTH_SHORT);
                } else {
                    sToast.setText(resId);
                }
                sToast.show();
            }
        });

    }

    public static void show(final String text) {
        JUI.post(new Runnable() {
            @Override
            public void run() {
                if (sToast == null) {
                    sToast = Toast.makeText(JApplication.getApp(), text, Toast.LENGTH_SHORT);
                } else {
                    sToast.setText(text);
                }
                sToast.show();
            }
        });
    }
}
