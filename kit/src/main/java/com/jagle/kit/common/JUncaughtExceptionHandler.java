package com.jagle.kit.common;

import android.os.Looper;

import com.jagle.kit.utils.JDeviceUtil;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.Thread.UncaughtExceptionHandler;

public class JUncaughtExceptionHandler implements UncaughtExceptionHandler {
    private UncaughtExceptionHandler mExceptionHandler;

    public JUncaughtExceptionHandler(UncaughtExceptionHandler defaultUncaughtExceptionHandler) {
        this.mExceptionHandler = defaultUncaughtExceptionHandler;
    }

    public void uncaughtException(final Thread thread, final Throwable throwable) {
        if (throwable == null) {
            return;
        }
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        throwable.printStackTrace(pw);
        final String str = sw.toString();

        if (JDeviceUtil.externalStorageWritable()) {
            try {
                JLogWriter.writeLogToFileReal(JLogWriter.getLogPath(), JLogWriter.UE_LOG_NAME, str);
            }
            catch (IOException e) {
                JLog.e(Developer.Jagle, "sdcard not exsit", e);
            }
        }

        //start
        final Runnable task = new Runnable() {
            public void run() {
            	//send to crash report server;

                if (mExceptionHandler != null) {
                    mExceptionHandler.uncaughtException(thread, throwable);
                }
            }
        };
        if (Thread.currentThread().getId() == Looper.getMainLooper().getThread().getId()) {
            new Thread(task).start();
        } else {
            task.run();
        }
    }
}
