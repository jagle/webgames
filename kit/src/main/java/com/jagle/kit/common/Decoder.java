package com.jagle.kit.common;

import android.util.Base64;

import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class Decoder {
	public static byte[] base64(byte[] input){
		return Base64.decode(input, Base64.DEFAULT);
	}
	
	public static byte[] base64(String input){
		return Base64.decode(input.getBytes(), Base64.DEFAULT);
	}
	
	public static byte[] hex(String input) {
		int length = input.length();
		byte[] result = new byte[length >> 1];
		for (int i = 0; i < length; i += 2) {
			result[i] = Integer.valueOf(input.substring(i, i + 2), 16)
					.byteValue();
		}
		return result;
	}

	public static byte[] aes(byte[] input, byte[] key, boolean secretKey) {
		try {
			if (secretKey){
				KeyGenerator kgen = KeyGenerator.getInstance("AES");    
		        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG", "Crypto");    
		        sr.setSeed(key);    
		        kgen.init(128, sr); // 192 and 256 bits may not be available    
		        SecretKey skey = kgen.generateKey();    
		        key = skey.getEncoded();  
			}
			
			SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
			Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.DECRYPT_MODE, skeySpec);
			return cipher.doFinal(input);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
