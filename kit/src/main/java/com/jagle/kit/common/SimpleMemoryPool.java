package com.jagle.kit.common;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

public class SimpleMemoryPool {
	
	private Queue<byte[]> mPool;
	
	private HashMap<byte[], Integer> mLockCount;
	
	public SimpleMemoryPool(int bufferSize, int bufferNum){
		mPool = new LinkedList<byte[]>();
		for (int i = 0; i < bufferNum ; ++i) {
			mPool.add(new byte[bufferSize]);
		}
		
		mLockCount = new HashMap<byte[], Integer>();
	}
	
	public synchronized byte[] getFreeBuffer(){
		return mPool.poll();
	}
	
	public synchronized void lockBuffer(byte[] buffer){
		Integer count = mLockCount.get(buffer);
		if (count == null) {
			mLockCount.put(buffer, 1);
		} else {
			mLockCount.put(buffer, ++count);
		}
	}
	
	public synchronized void unlockBuffer(byte[] buffer) { 
		Integer count = mLockCount.get(buffer);
		if (count == null || count <= 1) {
			mLockCount.put(buffer, 0);
			mPool.add(buffer);
		} else {
			mLockCount.put(buffer, --count);
		}
	}
	
	public synchronized void release() {
		mPool.clear();
		mLockCount.clear();
	}
	
}
