package com.jagle.kit.common;

public enum Developer {
	
	Default("App", "App", "627123877@qq.com"),
	Jagle("jagle", "liujie", "627123877@qq.com");
	
	public String mTag;
	public String mName;
	public String mEmail;
	
	private Developer(String tag, String name, String email){
		mTag = tag;
		mName = name;
		mEmail = email;
	}
	
	public String buildTag(String subTag){
		return mTag + " " + subTag;
	}

}
