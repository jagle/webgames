package com.jagle.kit.common;

import android.annotation.SuppressLint;
import android.os.Environment;
import android.util.Log;

import com.jagle.kit.base.JApplication;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ConcurrentLinkedQueue;

@SuppressLint("SimpleDateFormat")
public class JLogWriter {
	/// data members
	public static final int MAX_FILE_SIZE = 2;// M bytes
	public static final String LOG_PATH = "/jagle/app/logs";
	public static final String LOG_NAME = "logs.txt";
	public static final String UE_LOG_NAME = "uncaught_exception.txt";
	private static final SimpleDateFormat LOG_FORMAT = new SimpleDateFormat("yyyy:MM:dd kk:mm:ss.SSS");

	/// log struct
	public static class LogMsg{
		public String path;
		public String fileName;
		public String msg;
	}
	/// log container
	public static ConcurrentLinkedQueue<LogMsg> logMsgQueue = new ConcurrentLinkedQueue<LogMsg>();
	public static Runnable logRunnable = null;

	public static String getLogPath(){
		return "/jagle/" + JApplication.getApp().getPackageName() + "/logs";
	}

	/// push log
	public synchronized static void writeLogToFile(String path,
			String fileName, String msg) {
		LogMsg logMsg = new LogMsg();
		logMsg.path = path;
		logMsg.msg = msg;
		logMsg.fileName = fileName;
		
		logMsgQueue.add(logMsg);
		
		if (logRunnable == null) {
			logRunnable = new Runnable() {
			
				@Override
				public void run() {
					LogMsg logMsg = logMsgQueue.poll();
					while (logMsg != null) {
						try {
							writeLogToFileReal(logMsg.path, logMsg.fileName, logMsg.msg);
						} catch (IOException e) {
							e.printStackTrace();
						}
						logMsg = logMsgQueue.poll();
					}
					logRunnable = null;
				}
			};		
			new Thread(logRunnable).start();
		}
	}

	/// log to file
	@SuppressLint("SimpleDateFormat")
	public synchronized static void writeLogToFileReal(String path, String fileName, String msg)
			throws IOException {
		Date date = new Date();

		File esdf = Environment.getExternalStorageDirectory();
		String dir = esdf.getAbsolutePath() + path;
		File dirFile = new File(dir);
		if (!dirFile.exists()) {
			dirFile.mkdirs();
		}
		File logFile = new File(dir + File.separator + fileName);
		if (!logFile.exists()) {
			try {
				logFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
		} else {
			long fileSize = (logFile.length() >>> 20);// convert to M bytes
			if (fileSize > MAX_FILE_SIZE) {
			    deleteOldLogs();
			    
				SimpleDateFormat simpleDateFormate = new SimpleDateFormat("-MM-dd-kk-mm-ss");
				String fileExt = simpleDateFormate.format(date);

				StringBuilder sb = new StringBuilder(dir);
				sb.append(File.separator).append(fileName).append(fileExt)
						.append(".bak");

				File fileNameTo = new File(sb.toString());
				logFile.renameTo(fileNameTo);
			}
		}

		String strLog = LOG_FORMAT.format(date);

		StringBuffer sb = new StringBuffer(strLog);
		sb.append(' ');
		sb.append(msg);
		sb.append('\n');
		strLog = sb.toString();

		// we can make FileWriter static, but when to close it
		FileWriter fileWriter = new FileWriter(logFile, true);
		fileWriter.write(strLog);
		fileWriter.flush();

		fileWriter.close();
	}
	
    private static final long DAY_DELAY = 10 * 24 * 60 * 60 * 1000;

    private static void deleteOldLogs() {
        File esdf = Environment.getExternalStorageDirectory();
        if (!esdf.exists()) {
            return;
        }
        String dir = esdf.getAbsolutePath() + LOG_PATH;
        File dirFile = new File(dir);
        if (!dirFile.exists()) {
            return;
        }

        long now = System.currentTimeMillis();
        File files[] = dirFile.listFiles();
        for (File file : files) {
            if (file.getName().endsWith(".bak")) {
                long lastModifiedTime = file.lastModified();
                if (now - lastModifiedTime > DAY_DELAY) {
                    file.delete();
                }
            }

        }

    }
	
	private static final SimpleDateFormat FILE_NAME_FORMAT = new SimpleDateFormat(
			"MM-dd_HH-mm-ss");
	private static final String LOGCAT_CMD[] = { "logcat", "-d", "-v", "time" };

	// to use this method, we should add permission(android.permission.READ_LOGS) in Manifest
	public static void writeAllLogsToFile() {
		new Thread(new Runnable() {
			
			public void run() {
				try {
					Date date = new Date();
					Process process = Runtime.getRuntime().exec(LOGCAT_CMD);
					BufferedReader bufferedReader = new BufferedReader(
							new InputStreamReader(process.getInputStream()),
							1024);
					StringBuilder sb = new StringBuilder();
					String line;
					while ((line = bufferedReader.readLine()) != null) {
						sb.append(line);
						sb.append(System.getProperty("line.separator"));
					}
					bufferedReader.close();
					
					Log.i("huncent-jb", "all logs: "+sb.toString());

					File esdf = Environment.getExternalStorageDirectory();
					String dir = esdf.getAbsolutePath() + LOG_PATH;
					File dirFile = new File(dir);
					if (!dirFile.exists()) {
						dirFile.mkdirs();
					}
					String fileName = dir + File.separator + FILE_NAME_FORMAT.format(date) + ".log";
					File file = new File(fileName);
					if (!file.exists()) {
						file.createNewFile();
					}
					FileOutputStream fos = new FileOutputStream(file);
					fos.write(sb.toString().getBytes());
					fos.flush();
					fos.close();
				} catch (IOException e) {
					e.printStackTrace();
					Log.e("huncent-jb", "writeAllLogsToFile " + e.toString());
				}
			}
		}).start();
	}
}
