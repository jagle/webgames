package com.jagle.kit.common;

import android.os.Process;
import android.util.Log;

import com.jagle.kit.BuildConfig;
import com.jagle.kit.base.JApplication;
import com.jagle.kit.utils.JDeviceUtil;
import com.umeng.analytics.MobclickAgent;

import java.io.PrintWriter;
import java.io.StringWriter;

public class JLog {
	//change local
    public static void v(Developer developer, String format, Object ... args) {
    	if (JApplication.getApp().isDebug()) {
    		try {
    			String msg = String.format(format, args);
    			int line = getCallerLineNumber();
                String fileName = getCallerFilename();
                String methodName = getCallerMethodName();
                String className = getCallerClassName();
                String logText = msgForTextLog(msg, className, methodName, fileName, line);
    			Log.v(developer.buildTag(className), logText);
    		}
    		catch (java.util.IllegalFormatException e) {
    			e.printStackTrace();
    		}
    	}
    }
    
    public static void v(Developer developer, String msg) {
        if (JApplication.getApp().isDebug()) {
        	int line = getCallerLineNumber();
            String fileName = getCallerFilename();
            String methodName = getCallerMethodName();
            String className = getCallerClassName();
            String logText = msgForTextLog(msg, className, methodName, fileName, line);
            Log.v(developer.buildTag(className), logText);
        }
    }
    
    public static void verboseArray(Developer developer, String prefix, Object[] args){
    	if (JApplication.getApp().isDebug()){
    		StringBuilder sb = new StringBuilder(prefix);
    		for (Object arg : args){
    			sb.append("::");
    			sb.append(arg.toString());
    			sb.append("::@");
    		}
    		int line = getCallerLineNumber();
            String fileName = getCallerFilename();
            String methodName = getCallerMethodName();
            String className = getCallerClassName();
            String logText = msgForTextLog(sb.toString(), className, methodName, fileName, line);
            Log.v(developer.buildTag(className), logText);
    	}
    }
    
    public static void d(Developer developer, String format, Object ... args) {
    	if (JApplication.getApp().isDebug()) {
    		try {
    			String msg = String.format(format, args);
    			int line = getCallerLineNumber();
                String fileName = getCallerFilename();
                String methodName = getCallerMethodName();
                String className = getCallerClassName();
                String logText = msgForTextLog(msg, className, methodName, fileName, line);
    			Log.d(developer.buildTag(className), logText);
    			if (JDeviceUtil.externalStorageWritable())
    				logToFile(logText);
    		}
    		catch (java.util.IllegalFormatException e) {
    			e.printStackTrace();
    		}
    	}
    }
    
    public static String d(Developer developer, String msg) {
        if (JApplication.getApp().isDebug()) {
        	int line = getCallerLineNumber();
            String fileName = getCallerFilename();
            String methodName = getCallerMethodName();
            String className = getCallerClassName();
            String logText = msgForTextLog(msg, className, methodName, fileName, line);
            Log.d(developer.buildTag(className), logText);
            if (JDeviceUtil.externalStorageWritable())
                logToFile(logText);
        }
        return msg;
    }

    public static void d(Developer developer, String msg, Throwable t) {
        if (JApplication.getApp().isDebug()) {
            int line = getCallerLineNumber();
            String fileName = getCallerFilename();
            String methodName = getCallerMethodName();
            String className = getCallerClassName();
            String logText = msgForTextLog(msg, className, methodName, fileName, line);
            Log.d(developer.buildTag(className), logText, t);
            if (JDeviceUtil.externalStorageWritable())
                logToFile(logText, t);
        }
    }

    public static void i(Developer developer, String msg) {
    	int line = getCallerLineNumber();
        String fileName = getCallerFilename();
        String methodName = getCallerMethodName();
        String className = getCallerClassName();
        String logText = msgForTextLog(msg, className, methodName, fileName, line);
        Log.i(developer.buildTag(className), logText);
        if (JDeviceUtil.externalStorageWritable())
            logToFile(logText);
    }
    
    public static void i(Developer developer, String format, Object ... args) {
        try {
            String msg = String.format(format, args);
            int line = getCallerLineNumber();
            String fileName = getCallerFilename();
            String methodName = getCallerMethodName();
            String className = getCallerClassName();
            String logText = msgForTextLog(msg, className, methodName, fileName, line);
            Log.i(developer.buildTag(className), logText);
            if (JDeviceUtil.externalStorageWritable())
                logToFile(logText);
        }
        catch (java.util.IllegalFormatException e) {
            e.printStackTrace();
        }
    }

    public static void w(Developer developer, String format, Object ... args) {
        try {
            String msg = String.format(format, args);
            int line = getCallerLineNumber();
            String fileName = getCallerFilename();
            String methodName = getCallerMethodName();
            String className = getCallerClassName();
            String logText = msgForTextLog(msg, className, methodName, fileName, line);
            Log.w(developer.buildTag(className), logText);
            if (JDeviceUtil.externalStorageWritable())
                logToFile(logText);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void w(Developer developer, String msg) {
    	int line = getCallerLineNumber();
        String fileName = getCallerFilename();
        String methodName = getCallerMethodName();
        String className = getCallerClassName();
        String logText = msgForTextLog(msg, className, methodName, fileName, line);
        Log.w(developer.buildTag(className), logText);
        if (JDeviceUtil.externalStorageWritable())
            logToFile(logText);
    }

    public static void e(Developer developer, String format, Object ... args) {
        try {
            String msg = String.format(format, args);
            int line = getCallerLineNumber();
            String fileName = getCallerFilename();
            String methodName = getCallerMethodName();
            String className = getCallerClassName();
            String logText = msgForTextLog(msg, className, methodName, fileName, line);
            Log.e(developer.buildTag(className), logText);
            if (JDeviceUtil.externalStorageWritable())
                logToFile(logText);

            MobclickAgent.reportError(JApplication.getApp(), logText);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void e(Developer developer, String msg) {
    	int line = getCallerLineNumber();
        String fileName = getCallerFilename();
        String methodName = getCallerMethodName();
        String className = getCallerClassName();
        String logText = msgForTextLog(msg, className, methodName, fileName, line);
        Log.e(developer.buildTag(className), logText);

        if (JDeviceUtil.externalStorageWritable())
            logToFile(logText);

        MobclickAgent.reportError(JApplication.getApp(), logText);
    }

    public static void e(Developer developer,String msg, Throwable t) {
    	int line = getCallerLineNumber();
        String fileName = getCallerFilename();
        String methodName = getCallerMethodName();
        String className = getCallerClassName();
        String logText = msgForTextLog(msg, className, methodName, fileName, line);
        Log.e(developer.buildTag(className), logText, t);
        if (JApplication.getApp().isDebug() && JDeviceUtil.externalStorageWritable())
            logToFile(logText, t);

        MobclickAgent.reportError(JApplication.getApp(), logText);
    }
    
    private static void logToFile(String logText) {
        JLogWriter.writeLogToFile(JLogWriter.LOG_PATH, JLogWriter.LOG_NAME, logText);
    }

    private static void logToFile(String logText, Throwable t) {
        StringWriter sw = new StringWriter();
        sw.write(logText);
        sw.write("\n");
        t.printStackTrace(new PrintWriter(sw));
        JLogWriter.writeLogToFile(JLogWriter.getLogPath(), JLogWriter.LOG_NAME, sw.toString());
    }

    private static String msgForTextLog(String msg, String className, String methodName, String fileName, int line) {
        StringBuilder sb = new StringBuilder();
        sb.append(msg);
        sb.append(" (Class:");
        sb.append(className);
        sb.append(",");
        sb.append("Method:");
        sb.append(methodName);
        sb.append(")");
        sb.append(" (Pid:");
        sb.append(Process.myPid());
        sb.append(",");
        sb.append("Tid:");
        sb.append(String.format("%1$d %2$s", Process.myTid(), Thread.currentThread().getName()));
        sb.append(")");
        sb.append(" at (");
        sb.append(fileName);
        sb.append(":");
        sb.append(line);
        sb.append(")");
        String ret = sb.toString();
        return ret;
    }

    private static int getCallerLineNumber() {
        return Thread.currentThread().getStackTrace()[4].getLineNumber();
    }

    private static String getCallerFilename() {
        return Thread.currentThread().getStackTrace()[4].getFileName();
    }

    private static String getCallerMethodName() {
        return Thread.currentThread().getStackTrace()[4].getMethodName();
    }
    
    private static String getCallerClassName() {
        String name =  Thread.currentThread().getStackTrace()[4].getClassName();
        if (name.equals("<unknown class>")){
        	return name;
        }
        return name.substring(name.lastIndexOf('.') + 1);
    }
    
}
