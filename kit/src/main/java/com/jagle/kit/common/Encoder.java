package com.jagle.kit.common;

import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class Encoder {
	public static final String ALGORITHM_MD5 = "md5";
	public static final String ALGORITHM_SHA1 = "SHA-1";
	public static final String ALGORITHM_AES = "AES";
	public static final String ALGORITHM_HMAC_SHA1 = "HmacSHA1";
	
	public static final String DEFAULT_ENCODING = "UTF-8";
	
	public static String base64(String input, int flags) {
		try {
			return Base64.encodeToString(input.getBytes(DEFAULT_ENCODING),  flags);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static String base64(String input){
		return base64(input, Base64.NO_WRAP);
	}

	public static String base64(byte[] input, int flags) {
		return Base64.encodeToString(input, flags);
	}
	
	public static String base64(byte[] input){
		return base64(input, Base64.NO_WRAP);
	}

	public static byte[] md5(byte[] input) {
		return digest(input, "MD5");
	}

	public static byte[] md5(String input) {
		return md5(input.getBytes());
	}

	public static byte[] SHA1(byte[] input) {
		return digest(input, "SHA-1");
	}

	public static byte[] SHA1(String input) {
		return SHA1(input.getBytes());
	}

	public static final byte[] digest(byte[] input, String algorithm) {
		try {
			MessageDigest md = MessageDigest.getInstance(algorithm);
			return md.digest(input);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static byte[] hmacSha1(byte[] input, byte[] key) {
		try {
			SecretKeySpec sks = new SecretKeySpec(key, ALGORITHM_HMAC_SHA1);
			Mac mac = Mac.getInstance(ALGORITHM_HMAC_SHA1);
			mac.init(sks);
			return mac.doFinal(input);

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static byte[] hmacSha1(String input, String key) {
		try {
			return hmacSha1(input.getBytes(DEFAULT_ENCODING), key.getBytes(DEFAULT_ENCODING));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static byte[] aes(byte[] input, byte[] key, boolean secretKey) {
		try {
			if (secretKey){
				KeyGenerator kgen = KeyGenerator.getInstance("AES");    
		        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG", "Crypto");    
		        sr.setSeed(key);    
		        kgen.init(128, sr); // 192 and 256 bits may not be available    
		        SecretKey skey = kgen.generateKey();    
		        key = skey.getEncoded();  
			}
			
			SecretKeySpec skeySpec = new SecretKeySpec(key, "AES");
			Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
			byte[] encrypted = cipher.doFinal(input);
			return encrypted;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static byte[] aes(byte[] input, byte[] key){
		return aes(input, key, true);
	}

	public static String hex(byte[] input) {
		StringBuilder sb = new StringBuilder();
		for (byte b : input) {
			int unsignedB = b & 0xff;
			if (unsignedB < 0x10)
				sb.append("0");
			sb.append(Integer.toHexString(unsignedB));
		}
		return sb.toString();
	}

	public static String hex(String input) {
		return hex(input.getBytes());
	}
	
}
