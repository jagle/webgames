package com.jagle.kit.common;

import android.app.Activity;
import android.app.Fragment;
import android.view.View;
import android.view.View.OnClickListener;

import java.lang.ref.WeakReference;

/// use the weakreference to avoid circulate reference
public class ViewRef<T extends View> {
	
	/// data members
	private WeakReference<Object> mContainer = null; 
	private WeakReference<T> mHolderView;
	
	private final int ACTIVITY_CONATAINER = 0;
	private final int FRAGMENT_CONATAINER = 1;
	private final int VIEW_CONATAINER = 2;
	
	private final int mContainerType;
	private int mId;

	/// constructor from the activity with id
	public ViewRef(Activity container, int id) {
		mContainer = new WeakReference<Object>(container);
		mContainerType = ACTIVITY_CONATAINER;
		mId = id;
	}
	
	/// constructor from the root view with id
	public ViewRef(View container, int id) {
		mContainer = new WeakReference<Object>(container);
		mContainerType = VIEW_CONATAINER;
		mId = id;
	}
	
	public ViewRef(Fragment container, int id) {
		mContainer = new WeakReference<Object>(container);
		mContainerType = FRAGMENT_CONATAINER;
		mId = id;
	}

	@SuppressWarnings("unchecked")
	public T get() {
		if (mHolderView == null || mHolderView.get() == null) {
			try {
				mHolderView = new WeakReference<T>((T)findView());
			} catch (Exception e) {
				JLog.e(Developer.Jagle, "view not found id:" + mId, e);
			}
		}
		return mHolderView.get();
	}

	/// find view from root, if root is null, then from activity
	View findView() {
		switch (mContainerType) {
		case ACTIVITY_CONATAINER:{
			Activity act = (Activity) mContainer.get();
			return act.findViewById(mId);
		}
		case FRAGMENT_CONATAINER:{
			Fragment frg = (Fragment) mContainer.get();
			return frg.getView().findViewById(mId);
		}
		case VIEW_CONATAINER:{
			View view = (View) mContainer.get();
			return view.findViewById(mId);
		}
		default:
			return null;
		}
	}

	public void setOnClickListener(OnClickListener onClickListener) {
		if (get() != null){
			get().setOnClickListener(onClickListener);
		} else {
			JLog.d(Developer.Jagle, "view not found id:" + mId);
		}
	}
	
	public void setVisibility(int visibility){
		if (get() != null){
			get().setVisibility(visibility);
		} else {
			JLog.d(Developer.Jagle, "view not found id:" + mId);
		}
	}
	
	public void setPadding(int left, int top, int right, int bottom) {
		if (get() != null){
			get().setPadding(left, top, right, bottom);
		} else {
			JLog.d(Developer.Jagle, "view not found id:" + mId);
		}
	}
	
	public boolean exist(){
		return get() != null;
	}
	
}
