package com.jagle.kit.property;

import java.util.List;

/**
 * Created by jagle on 15/9/17.
 */
public interface IListPropertyListener<T> {
    public void onDataChanged(List<T> data);
}
