package com.jagle.kit.property;

import java.util.List;

/**
 * Created by jagle on 15/10/28.
 */
public interface IValuePropertyListener<T> {
    public void onDataChanged(T data);
}
