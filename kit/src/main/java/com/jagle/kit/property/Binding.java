package com.jagle.kit.property;

import android.renderscript.Sampler;

import com.jagle.kit.common.Developer;
import com.jagle.kit.common.JLog;

import java.util.HashMap;
import java.util.List;


public class Binding {
	
	private HashMap<IListPropertyListener<?>, IListProperty.IListValueChangeHandler<?>> mListPropertyMap;
	private HashMap<IValuePropertyListener<?>, IValueProperty.IValueChangeHandler<?>> mValuePropertyMap;
	
	private Binding() {
		mListPropertyMap = new HashMap<IListPropertyListener<?>, IListProperty.IListValueChangeHandler<?>>();
		mValuePropertyMap = new HashMap<>();
	}
	
	private static class InstanceCls{
		private static Binding mInstance = new Binding();
	}
	
	public static Binding getInstance(){
		return InstanceCls.mInstance;
	}

	private <T> void inerBind(ListProperty<T> data, final IListPropertyListener<T> adapter ) {
		if (data == null || adapter == null) {
			throw new IllegalArgumentException("data or adapter is null");
		}

		@SuppressWarnings("unchecked")
		IListProperty.IListValueChangeHandler<T> old = (IListProperty.IListValueChangeHandler<T>) mListPropertyMap.get(adapter);
		if (old != null) {
			data.removePropChangeHandler(old);
		}

		IListProperty.IListValueChangeHandler<T> handler = new IListProperty.IListValueChangeHandler<T>() {
			@Override
			public void onPropChange(List<T> value) {
				adapter.onDataChanged(value);
			}
		};

		mListPropertyMap.put(adapter, handler);
		data.addPropChangeHandler(handler);
		adapter.onDataChanged(data.get());
	}

	public static <T> void bind(ListProperty<T> data, IListPropertyListener<T> adapter) {
		getInstance().inerBind(data, adapter);
	}

	private <T> void inerUnbind(ListProperty<T> data, final IListPropertyListener<T> adapter ) {
		if (data == null || adapter == null) {
			throw new IllegalArgumentException("data or adapter is null");
		}

		@SuppressWarnings("unchecked")
		IListProperty.IListValueChangeHandler<T> old = (IListProperty.IListValueChangeHandler<T>) mListPropertyMap.get(adapter);
		if (old == null) {
			JLog.e(Developer.Jagle, "no bind before");
		} else {
			data.removePropChangeHandler(old);
		}

		mListPropertyMap.remove(adapter);
	}

	public static <T> void unbind(ListProperty<T> data, IListPropertyListener<T> adapter) {
		getInstance().inerUnbind(data, adapter);
	}
	
	private <T> void inerBind(ValueProperty<T> data, final IValuePropertyListener<T> adapter ) {
		if (data == null || adapter == null) {
			throw new IllegalArgumentException("data or adapter is null");
		}
		
		@SuppressWarnings("unchecked")
		IValueProperty.IValueChangeHandler<T> old = (IValueProperty.IValueChangeHandler<T>) mValuePropertyMap.get(adapter);
		if (old != null) {
			data.removePropChangeHandler(old);
		}

		IValueProperty.IValueChangeHandler<T> handler = new IValueProperty.IValueChangeHandler<T>() {
			@Override
			public void onPropChange(T value) {
				adapter.onDataChanged(value);
			}
		};

		mValuePropertyMap.put(adapter, handler);
		data.addPropChangeHandler(handler);
		adapter.onDataChanged(data.get());
	}
	
	public static <T> void bind(ValueProperty<T> data, final IValuePropertyListener<T> adapter) {
		getInstance().inerBind(data, adapter);
	}
	
	private <T> void inerUnbind(ValueProperty<T> data, final IValuePropertyListener<T> adapter  ) {
		if (data == null || adapter == null) {
			throw new IllegalArgumentException("data or adapter is null");
		}
		
		@SuppressWarnings("unchecked")
		IValueProperty.IValueChangeHandler<T> old = (IValueProperty.IValueChangeHandler<T>) mValuePropertyMap.get(adapter);
		if (old == null) {
			JLog.e(Developer.Jagle, "no bind before");
		} else {
			data.removePropChangeHandler(old);
		}

		mValuePropertyMap.remove(adapter);
	}
	
	public static <T> void unbind(ValueProperty<T> data, final IValuePropertyListener<T> adapter) {
		getInstance().inerUnbind(data, adapter);
	}

}
