package com.jagle.kit.property;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;

public class DataCenter {
	
	private HandlerThread mHandlerThread;
	private H mHandler;
	
	private static class InstanceCls {
		private static DataCenter mInstance = new DataCenter();
	}
	
	public static DataCenter  getInstance() {
		return InstanceCls.mInstance;
	}
	
	private DataCenter() {
		mHandlerThread = new HandlerThread("Database Op thread");
		mHandlerThread.start();
		mHandler = new H(this, mHandlerThread.getLooper());
	}
	
	public static void post(Runnable runnable) {
		getInstance().mHandler.post(runnable);
	}
	
	public static void postDelay(Runnable runnable , long delayed){
		getInstance().mHandler.postDelayed(runnable, delayed);
	}
	
	private static class H extends Handler {
		public H(DataCenter dc, Looper looper) {
			super(looper);
		}
	}
	
}
