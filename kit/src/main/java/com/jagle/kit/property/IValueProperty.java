package com.jagle.kit.property;

/**
 * @author qingfeng
 */
public interface IValueProperty<T> {
	public static interface IValueChangeHandler<T> {
		public void onPropChange(T value);
	}
	public T get();
	public void set(T v);
	public void reset();

	public void addPropChangeHandler(IValueChangeHandler<T> handler);
	public void removePropChangeHandler(IValueChangeHandler<T> handler);
}
