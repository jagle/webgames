package com.jagle.kit.property;

import java.util.ArrayList;
import java.util.List;

import android.os.Looper;

import com.jagle.kit.common.Developer;
import com.jagle.kit.common.JLog;
import com.jagle.kit.utils.JUI;


/**
 * Created by liujie on 14-10-13.
 */
public class ListProperty<T> implements IListProperty<T>  {

    protected List<T> mValue;
    protected List<T> mDefaultValue;

    private final ArrayList<IListValueChangeHandler<T>> mHandlers = new ArrayList<IListValueChangeHandler<T>>();

    public ListProperty(List<T> defaultValue) {
        mDefaultValue = defaultValue;
        reset();
    }

    public ListProperty() {
        mDefaultValue = new ArrayList<T>();
        reset();
    }

    @Override
    public synchronized List<T> get() {
        return mValue;
    }
    
    public synchronized void set(int i, T obj) {
    	if (mValue == null) {
            JLog.e(Developer.Jagle, "mValue is null");
            return ;
        }

        mValue.set(i, obj);

        if (Looper.getMainLooper() != Looper.myLooper()) {
            JUI.post(new Runnable() {
                @Override
                public void run() {
                    onPropChange(mValue);
                }
            });
        } else {
            onPropChange(mValue);
        }
    }

    @Override
    public synchronized void set(List<T> value) {
        if (null != mValue && mValue.equals(value))
            return;
        mValue = value;
        if (Looper.getMainLooper() != Looper.myLooper()) {
            JUI.post(new Runnable() {
                @Override
                public void run() {
                    onPropChange(mValue);
                }
            });
        } else {
            onPropChange(mValue);
        }
    }

    @Override
    public synchronized void add(T obj) {
        if (mValue == null) {
            JLog.e(Developer.Jagle, "mValue is null");
            return ;
        }

        mValue.add(obj);

        if (Looper.getMainLooper() != Looper.myLooper()) {
            JUI.post(new Runnable() {
                @Override
                public void run() {
                    onPropChange(mValue);
                }
            });
        } else {
            onPropChange(mValue);
        }
    }

    @Override
    public synchronized void add(int i, T obj) {
        if (mValue == null){
            JLog.e(Developer.Jagle, "mValue is null");
            return ;
        }

        mValue.add(i, obj);

        if (Looper.getMainLooper() != Looper.myLooper()) {
            JUI.post(new Runnable() {
                @Override
                public void run() {
                    onPropChange(mValue);
                }
            });
        } else {
            onPropChange(mValue);
        }
    }

    @Override
    public synchronized void remove(T obj) {
    	 if (mValue == null){
             JLog.e(Developer.Jagle, "mValue is null");
             return ;
         }

         mValue.remove(obj);

         if (Looper.getMainLooper() != Looper.myLooper()) {
             JUI.post(new Runnable() {
                 @Override
                 public void run() {
                     onPropChange(mValue);
                 }
             });
         } else {
             onPropChange(mValue);
         }

    }

    @Override
    public synchronized void remove(int i) {
        if (mValue == null){
            JLog.e(Developer.Jagle, "mValue is null");
            return ;
        }

        mValue.remove(i);

        if (Looper.getMainLooper() != Looper.myLooper()) {
            JUI.post(new Runnable() {
                @Override
                public void run() {
                    onPropChange(mValue);
                }
            });
        } else {
            onPropChange(mValue);
        }
    }

    @Override
    public synchronized void reset() {
        set(mDefaultValue);
    }

    @Override
    public void addPropChangeHandler(IListValueChangeHandler<T> handler) {
        synchronized (mHandlers) {
            mHandlers.add(handler);
        }
    }

    @Override
    public void removePropChangeHandler(IListValueChangeHandler<T> handler) {
        synchronized (mHandlers) {
            mHandlers.remove(handler);
        }
    }

    private void onPropChange(List<T> value) {
        for (IListValueChangeHandler<T> handler : mHandlers) {
            handler.onPropChange(value);
        }
    }
}
