package com.jagle.kit.property;

import java.util.ArrayList;

import android.os.Looper;

import com.jagle.kit.utils.JUI;


/**
 */
public class ValueProperty<T> implements IValueProperty<T> {
    private final ArrayList<IValueChangeHandler<T>> mHandlers = new ArrayList<IValueChangeHandler<T>>();
    private T mValue;
    private T mDefaultValue;

    public ValueProperty(T defaultValue) {
        mDefaultValue = defaultValue;
        reset();
    }

    public ValueProperty() {
        mDefaultValue = null;
        reset();
    }

    public synchronized T get() {
        return mValue;
    }

    public synchronized void set(final T value) {
        if (null != mValue && mValue.equals(value))
            return;
        mValue = value;
        if (Looper.getMainLooper() != Looper.myLooper()) {
            JUI.post(new Runnable() {
                @Override
                public void run() {
                    onPropChange(value);
                }
            });
        } else {
            onPropChange(value);
        }
    }

    private void onPropChange(T value) {
        for (IValueChangeHandler<T> onChangedHandler : mHandlers) {
			onChangedHandler.onPropChange(value);
		}
    }

    public void reset() {
        set(mDefaultValue);
    }

    public void addPropChangeHandler(IValueChangeHandler<T> handler) {
        synchronized (mHandlers) {
            mHandlers.add(handler);
        }
    }

    public void removePropChangeHandler(IValueChangeHandler<T> handler) {
        synchronized (mHandlers) {
            mHandlers.remove(handler);
        }
    }
}
