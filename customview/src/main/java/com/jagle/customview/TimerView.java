package com.jagle.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.jagle.kit.utils.JTimeUtils;

/**
 * Created by liujie on 15-1-21.
 */
public class TimerView extends TextView {
    private long mStartTime = 0;

    public TimerView(Context context) {
        super(context);
    }

    public TimerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TimerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public TimerView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void stop(){
        removeCallbacks(mTimerRunable);
    }

    public void start() {
        mStartTime = System.currentTimeMillis();
        post(new Runnable() {
            @Override
            public void run() {
                mTimerRunable.run();
            }
        });
    }

    private void updateTime() {
        Long diff = System.currentTimeMillis() - mStartTime;
        setText(JTimeUtils.getMillisHMSFormat(diff));
    }

    private Runnable mTimerRunable = new Runnable() {
        @Override
        public void run() {
            updateTime();
            postDelayed(mTimerRunable, 1000);
        }
    };

}
