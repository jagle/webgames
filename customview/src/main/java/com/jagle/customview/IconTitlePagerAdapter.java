package com.jagle.customview;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public abstract class IconTitlePagerAdapter extends FragmentPagerAdapter {

	public IconTitlePagerAdapter(FragmentManager fm) {
		super(fm);
	}
	
	abstract public String getTitle(int position);
	
	abstract public int getIcon(int position);

}
