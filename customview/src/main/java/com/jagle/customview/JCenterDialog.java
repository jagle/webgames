package com.jagle.customview;


import android.content.Context;
import android.view.Gravity;
import android.view.WindowManager.LayoutParams;

public class JCenterDialog extends JDialog {

	public JCenterDialog(Context context) {
		super(context, R.style.JCenterDialog);
		
		getWindow().setLayout(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		getWindow().setGravity(Gravity.CENTER);
		setCanceledOnTouchOutside(true);
	}

}
