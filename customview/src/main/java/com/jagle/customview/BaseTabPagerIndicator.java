package com.jagle.customview;


import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.jagle.kit.utils.JUI;

public abstract class BaseTabPagerIndicator extends LinearLayout implements PagerIndicator {
	private ViewPager mViewPager;
	
	private LayoutInflater mInflater;
	private int mSelectedTabIndex = 0;
	
	private ViewPager.OnPageChangeListener mListener;

	protected OnTabReselectedListener mTabReselectedListener;

	public BaseTabPagerIndicator(Context context, AttributeSet attrs) {
		super(context, attrs);

		mInflater = LayoutInflater.from(context);
	}

	public BaseTabPagerIndicator(Context context) {
		super(context);

		mInflater = LayoutInflater.from(context);
	}

	public View getTabView(int index ,int layoutId){
		return getTabView(index, layoutId, 0);
	}

	public View getTabView(int index, int layoutId, int weiget){
		View v =  mInflater.inflate(layoutId, null);
		int width , height;
		if (weiget != 0){
			if (getOrientation() == HORIZONTAL){
				width = 0;
				height = JUI.MATCH_PARENT;
			} else {
				width = JUI.MATCH_PARENT;
				height = 0;
			}

			v.setLayoutParams(new LayoutParams(width, height, weiget));
		}
		return v;
	}

	public void addTab(final int index, View v){
		v.setClickable(true);
		v.setOnClickListener(new OnClickListener() {
	        public void onClick(View view) {
	            final int oldSelected = mViewPager.getCurrentItem();
	            final int newSelected = index;
	            mViewPager.setCurrentItem(newSelected);
	            if (oldSelected == newSelected && mTabReselectedListener != null) {
	                mTabReselectedListener.onTabReselected(newSelected);
	            }
	        }
	    });
		
		if (getOrientation() == HORIZONTAL){
			addView(v, new LayoutParams(0, LayoutParams.MATCH_PARENT, 1));
		} else {
			addView(v, new LayoutParams(LayoutParams.MATCH_PARENT, 0, 1));
		}
	}
	
	public void setOnTabReselectedListener(OnTabReselectedListener listener){
		mTabReselectedListener = listener;
	}
	
	@Override
	public void onPageScrollStateChanged(int arg0) {
		if (mListener != null){
			mListener.onPageScrollStateChanged(arg0);
		}
	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
		if (mListener != null){
			mListener.onPageScrolled(arg0, arg1, arg2);
		}
	}

	@Override
	public void onPageSelected(int arg0) {
		setCurrentItem(arg0);
		if (mListener != null){
			mListener.onPageSelected(arg0);
		}
	}

	@Override
	public void setViewPager(ViewPager view, int initialPosition) {
		setViewPager(view);
        setCurrentItem(initialPosition);
	}

	@Override
	public void setCurrentItem(int item) {
		if (mViewPager == null) {
            throw new IllegalStateException("ViewPager has not been bound.");
        }
		
        mSelectedTabIndex = item;
        mViewPager.setCurrentItem(item);

        final int tabCount = getChildCount();
        for (int i = 0; i < tabCount; i++) {
            final View child = getChildAt(i);
            final boolean isSelected = (i == item);
            child.setSelected(isSelected);
        }
	}

	@Override
	public void setOnPageChangeListener(OnPageChangeListener listener) {
		mListener = listener;
	}

	@Override
	public void notifyDataSetChanged() {
		removeAllViews();
        PagerAdapter adapter = mViewPager.getAdapter();
   
        final int count = adapter.getCount();
        for (int i = 0; i < count; i++) {
            addTab(i, getIndicatorView(i, mViewPager));
        }
        if (mSelectedTabIndex > count) {
            mSelectedTabIndex = count - 1;
        }
        setCurrentItem(mSelectedTabIndex);
        requestLayout();
	}


	@Override
	public void setViewPager(ViewPager view) {
		if (mViewPager == view) {
            return;
        }
        if (mViewPager != null) {
            mViewPager.setOnPageChangeListener(null);
        }
        final PagerAdapter adapter = view.getAdapter();
        if (adapter == null) {
            throw new IllegalStateException("ViewPager does not have adapter instance.");
        }
        mViewPager = view;
        view.setOnPageChangeListener(this);
        notifyDataSetChanged();
	}
	
	public interface OnTabReselectedListener {
        /**
         * Callback when the selected tab has been reselected.
         *
         * @param position Position of the current center item.
         */
        void onTabReselected(int position);
    }

	public abstract View getIndicatorView(int index, ViewPager pager);
}
