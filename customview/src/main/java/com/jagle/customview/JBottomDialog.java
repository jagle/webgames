package com.jagle.customview;


import android.content.Context;
import android.view.Gravity;
import android.view.WindowManager.LayoutParams;

public class JBottomDialog extends JDialog {

	public JBottomDialog(Context context) {
		super(context, R.style.JBottomDialog);
		
		getWindow().setLayout(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		getWindow().setGravity(Gravity.BOTTOM);
		setCanceledOnTouchOutside(true);
	}

}
