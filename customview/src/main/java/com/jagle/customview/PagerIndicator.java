package com.jagle.customview;

import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;

public interface PagerIndicator extends OnPageChangeListener {
	 void setViewPager(ViewPager pager);
	 
	 void setViewPager(ViewPager pager, int initialPosition);
	 
	 void setCurrentItem(int item);
	 
	 void setOnPageChangeListener(ViewPager.OnPageChangeListener listener);
	 
	 void notifyDataSetChanged();
	 
}
