package com.jagle.customview;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.GridView;
import android.widget.ListAdapter;

import com.jagle.kit.utils.JUI;

public class ImageGridView extends GridView {
	
	private int mItemHeigth = JUI.WRAP_CONCENT;
	
	private boolean mAutoHeight = false;
	private boolean mAutoResizeItem = false;
	
	private int mColumnNum = 4;
	private int mSpacing = 20;
	
	public ImageGridView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		
		init(attrs);
		initGridView();
	}

	public ImageGridView(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		init(attrs);
		initGridView();
	}
	
	public void setSpacing(int spacing){
		mSpacing = spacing;
		setHorizontalSpacing(spacing);
		setVerticalSpacing(spacing);
	}
	
	public void setAutoResizeItem(boolean autoResize){
		mAutoResizeItem =autoResize;
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
	
		if (mAutoHeight){
			int newSpec = MeasureSpec.makeMeasureSpec(MEASURED_SIZE_MASK, MeasureSpec.AT_MOST);
			super.onMeasure(widthMeasureSpec, newSpec);
		} else {
			super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		}
		
		ViewGroup.LayoutParams lp = getLayoutParams();
		int height = getMeasuredHeight();
		if (lp.height != height){
			lp.height = height;
			requestLayout();
		}
	}
	
	@Override
	public void setAdapter(ListAdapter adapter) {
		super.setAdapter(adapter);
		
		if (adapter instanceof ImageListAdapter){
			((ImageListAdapter) adapter).setItemHeight(mItemHeigth);
		}
	}
	
	public ImageGridView(Context context) {
		super(context);
		
		initGridView();
	}
	
	private void initGridView() {
		getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
			@Override
			public void onGlobalLayout() {
				if (!mAutoResizeItem){
					return ;
				}
				
				Rect padding = new Rect();
				Drawable d = getSelector();
				if (d != null){
					d.getPadding(padding);
				}
				int listPaddingLeft = padding.left + getPaddingLeft();
				int listPaddingRight = padding.right + getPaddingRight();
		        
				int width = getWidth() - listPaddingLeft - listPaddingRight;
				int columnsWidth = (width - (mColumnNum - 1) * mSpacing )/ mColumnNum;
				ListAdapter adapter = getAdapter();
				if (adapter != null && adapter instanceof ImageListAdapter){
					((ImageListAdapter) adapter).setItemHeight(columnsWidth);
				}
			}
		});
	}
	
	private void init(AttributeSet attrs) {
		TypedArray array = getContext().obtainStyledAttributes(attrs,
				R.styleable.ImageGridView);
		mItemHeigth = array.getDimensionPixelSize(
				R.styleable.ImageGridView_itemHeight, mItemHeigth);
		mAutoHeight = array.getBoolean(R.styleable.ImageGridView_autoHeight, mAutoHeight);
		array.recycle();
		
		ListAdapter adapter = getAdapter();
		if (adapter != null && adapter instanceof ImageListAdapter){
			((ImageListAdapter) adapter).setItemHeight(mItemHeigth);
		}
	}
}
