package com.jagle.threesdk;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.jagle.kit.common.Developer;
import com.jagle.kit.common.JLog;
import com.jagle.kit.property.DataCenter;


/**
 * Created by liujie on 14-8-25.
 */
public abstract class JOrmliteHelper extends OrmLiteSqliteOpenHelper {

	// name of the database file for your application -- change to something
	// appropriate for your app
	private static final String DATABASE_NAME = "Samples.db";
	// any time you make changes to your database objects, you may have to
	// increase the database version
	private static final int DATABASE_VERSION = 3;

	private static JOrmliteHelper mSelf;

	public JOrmliteHelper(Context context, String databaseName, SQLiteDatabase.CursorFactory factory,
						  int databaseVersion) {
		super(context, databaseName, factory, databaseVersion);

		mSelf = this;
	}

	@Override
	public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
		JLog.i(Developer.Jagle, "Database onCreate");
	}

	public static JOrmliteHelper getSelf(){
		return mSelf;
	}

	@Override
	public void onUpgrade(final SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
		JLog.i(Developer.Jagle, "Database onUpgrade");
	}


	public void init() {
		DataCenter.post(new Runnable() {
			@Override
			public void run() {
				initTables();
			}
		});
	}

	abstract protected void initTables();

	@Override
	public void close() {
		super.close();
	}
	
}
