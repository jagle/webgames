package com.jagle.threesdk;

import android.content.Context;

import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;

import java.io.File;

public class JImageLoaderSdk {
    public static void init(Context context) {
        // do adapter
        int cacheSize = (int) Runtime.getRuntime().maxMemory() / 8;
        File cacheDir = context.getApplicationContext().getCacheDir();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                context.getApplicationContext()).threadPoolSize(3)
                // default
                .threadPriority(Thread.NORM_PRIORITY - 1)
                        // default
                .denyCacheImageMultipleSizesInMemory()
                .memoryCache(new LruMemoryCache(cacheSize))
                        // prefer lru cache
                .imageDownloader(
                        new BaseImageDownloader(context.getApplicationContext())) // default
                .tasksProcessingOrder(QueueProcessingType.FIFO) // default
                .build();

        ImageLoader.getInstance().init(config);
    }
}
