package com.jagle.threesdk;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.Uri;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.jagle.kit.utils.JDimensUtil;
import com.jagle.threesdk.R;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.LoadedFrom;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.decode.BaseImageDecoder;
import com.nostra13.universalimageloader.core.display.BitmapDisplayer;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.core.imageaware.ImageAware;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.utils.StorageUtils;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public class AsyncImageView extends ImageView {

    private DisplayImageOptions.Builder mOptionBuilder;
    private DisplayImageOptions mOptions;
    private boolean mOptionInvalid = true;

    private int mLoadingImage = 0;
    private float mRoundPixelsPre = 1.0f;
    private boolean mRoundImage = false;
    private boolean mCacheInDisk = true;
    private boolean mImageOnLoading = true;

    private int mMaxRoundImageSize = 100;

    private ImageLoadingListener mListener;
    private ImageLoader mLoader;

    private Uri mUri;

    public AsyncImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    public AsyncImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public AsyncImageView(Context context) {
        super(context);
        init(null);
    }

    public void setStubImage(int resid) {
        if (mLoadingImage == resid) {
            return;
        }

        mLoadingImage = resid;
        mOptionInvalid = true;
    }

    public ImageLoader getLoader() {
        if (mLoader == null) {
            mLoader = ImageLoader.getInstance();
        }

        return mLoader;
    }

    public void setRoundPixels(float roundPixelsPre) {
        if (mRoundPixelsPre == roundPixelsPre) {
            return;
        }

        mRoundPixelsPre = roundPixelsPre;
        mOptionInvalid = true;
    }

    public void setRoundImage(boolean round) {
        if (mRoundImage == round) {
            return;
        }

        mRoundImage = round;
        mOptionInvalid = true;
    }

    public Uri getImageUri() {
        return mUri;
    }

    public void setCacheInDisk(boolean cache) {
        if (mCacheInDisk == cache) {
            return;
        }

        mCacheInDisk = cache;
        mOptionInvalid = true;
    }

    public void setImageLoadingListener(ImageLoadingListener listener) {
        mListener = listener;
    }

    public void setDisplayer(BitmapDisplayer displayer) {
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .cloneFrom(mOptions).displayer(displayer).
                        imageScaleType(ImageScaleType.NONE).build();
        mOptions = options;
    }

    private void resetOptionBuilder() {
        mOptionBuilder = new DisplayImageOptions.Builder()
                .bitmapConfig(Config.RGB_565)
                .displayer(new SimpleBitmapDisplayer()).cacheInMemory(true);
    }

    private void init(AttributeSet attrs) {
        mMaxRoundImageSize = JDimensUtil.dip2px(getContext(), 100);
        resetOptionBuilder();

        initAttrs(attrs);
        updateOption();
    }

    private void initAttrs(AttributeSet attrs) {
        if (attrs == null) {
            return;
        }
        TypedArray array = getContext().obtainStyledAttributes(attrs,
                R.styleable.AsyncImageView);
        int resid = array.getResourceId(R.styleable.AsyncImageView_stubImage,
                -1);
        if (resid != -1) {
            setStubImage(resid);
        }

        mRoundImage = array.getBoolean(R.styleable.AsyncImageView_roundImage, mRoundImage);
        mRoundPixelsPre = array.getFloat(R.styleable.AsyncImageView_roundPixelsPre, mRoundPixelsPre);
        mCacheInDisk = array.getBoolean(R.styleable.AsyncImageView_cacheInDisk, mCacheInDisk);
        mImageOnLoading = array.getBoolean(R.styleable.AsyncImageView_displayStubImageInLoading, mImageOnLoading);
        array.recycle();

        if (resid != -1) {
            setImageResource(resid);
        }
    }

    public void setDisplayStubImageInLoading(boolean isDisplaySubImageInLoading) {
        mImageOnLoading = isDisplaySubImageInLoading;
    }

    public void clear() {
        mUri = null;

        if (mLoader != null) {
            mLoader.cancelDisplayTask(this);
        }
    }

    @Override
    public void setImageURI(Uri uri) {
        if (mUri != null && mUri.compareTo(uri) == 0) {
            return;
        }

        updateOption();
        clear();
        mUri = uri;
        if (uri != null && !uri.toString().isEmpty()) {
            String decodeStr = null;
            try {
                decodeStr = URLDecoder.decode(uri.toString(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            getLoader().displayImage(decodeStr, this,
                    mOptions, mListener);
        } else if (mLoadingImage != 0) {
            setImageResource(mLoadingImage);
        }
    }

    public void setImageURI(String uriString) {
        if (uriString == null) {
            setImageURI(Uri.EMPTY);
        } else {
            if (uriString.startsWith("/")) {
                setImageURI(Uri.fromFile(new File(uriString)));
            } else {
                setImageURI(Uri.parse(uriString));
            }
        }
    }

    private void updateOption() {
        if (mOptionInvalid) {

            mOptionBuilder.cacheOnDisk(mCacheInDisk);

            BitmapDisplayer displayer = null;
            if (mRoundImage) {
                displayer = new JRoundedBitmapDisplayer(mRoundPixelsPre, mMaxRoundImageSize);
            } else {
                displayer = new SimpleBitmapDisplayer();
            }

            if (mImageOnLoading) {
                mOptionBuilder.showImageOnLoading(mLoadingImage);
            }

            mOptions = mOptionBuilder.
                    displayer(displayer).build();
            mOptionInvalid = false;
        }

    }

    static class JRoundedBitmapDisplayer implements BitmapDisplayer {
        private float mRoundPixelsPre;
        private int mMaxRoundSize;

        public JRoundedBitmapDisplayer(float roundPixelPre, int maxRoundSize) {
            mRoundPixelsPre = roundPixelPre;
            mMaxRoundSize = maxRoundSize;
        }

        private static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int roundPixels, Rect srcRect, Rect destRect, int width, int height) {
            Bitmap output = null;
            try {
                output = Bitmap.createBitmap(width, height, Config.ARGB_8888);
            } catch (Exception e) {
                e.printStackTrace();
                return bitmap;
            }

            Canvas canvas = new Canvas(output);

            final Paint paint = new Paint();
            final RectF destRectF = new RectF(destRect);

            paint.setAntiAlias(true);

            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(Color.BLACK);
            canvas.drawRoundRect(destRectF, roundPixels, roundPixels, paint);
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            canvas.drawBitmap(bitmap, srcRect, destRectF, paint);

            return output;
        }

        @Override
        public void display(Bitmap bitmap, ImageAware view,
                            LoadedFrom loadedFrom) {
            Rect srcRect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
            Rect destRect = srcRect;
            int bw = bitmap.getWidth();
            int bh = bitmap.getHeight();
            int min = Math.min(bw, bh);
            int destSize = Math.min(min, mMaxRoundSize);
            switch (view.getScaleType()) {
                case CROP:
                    int cutW = (bw - min) >> 1;
                    int cutH = (bh - min) >> 1;
                    srcRect = new Rect(cutW, cutH, bw - cutW, bh - cutH);
                    destRect = new Rect(0, 0, destSize, destSize);
                    break;
                default:
                    srcRect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
                    destRect = srcRect;
                    break;
            }

            int roundPixels = (int) (mRoundPixelsPre * Math.min(destRect.width(), destRect.height()));
            Bitmap rBitmap = getRoundedCornerBitmap(bitmap, roundPixels, srcRect, destRect, destRect.width(), destRect.height());
            view.setImageBitmap(rBitmap);
        }

    }

    ;

    public static void initConfig(Context context) {
        File cacheDir = StorageUtils.getCacheDirectory(context);
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .memoryCacheExtraOptions(480, 800) // default = device screen dimensions
                .diskCacheExtraOptions(480, 800, null)
                .threadPoolSize(3) // default
                .threadPriority(Thread.NORM_PRIORITY - 2) // default
                .tasksProcessingOrder(QueueProcessingType.FIFO) // default
                .denyCacheImageMultipleSizesInMemory()
                .memoryCache(new LruMemoryCache(10 * 1024 * 1024))
                .memoryCacheSize(10 * 1024 * 1024)
                .memoryCacheSizePercentage(13) // default
                .diskCacheSize(50 * 1024 * 1024)
                .diskCacheFileCount(100)
                .diskCacheFileNameGenerator(new HashCodeFileNameGenerator()) // default
                .imageDownloader(new BaseImageDownloader(context)) // default
                .defaultDisplayImageOptions(DisplayImageOptions.createSimple()) // default
                .writeDebugLogs()
                .build();
        ImageLoader.getInstance().init(config);
    }


}