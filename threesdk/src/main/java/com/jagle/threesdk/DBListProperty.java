package com.jagle.threesdk;

import com.j256.ormlite.android.DatabaseTableConfigUtil;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.table.DatabaseTableConfig;
import com.j256.ormlite.table.TableUtils;
import com.jagle.kit.property.DataCenter;
import com.jagle.kit.property.IDBProperty;
import com.jagle.kit.property.ListProperty;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Callable;


/**
 * Created by liujie on 14-10-13.
 */
public abstract class DBListProperty<T, K> extends ListProperty<T> implements IDBProperty {
    private Class<T> mClazz;
    private String mTableName;

    private HashMap<K, Integer> mIndexMap;

    abstract public K getKey(T data);

    public DBListProperty(Class<T> cls, String tableName) {
        super();
        mClazz = cls;
        mIndexMap = new HashMap<K, Integer>();
        if (tableName != null) {
            mTableName = tableName + "_v" + getVerison();
        }

        DataCenter.post(new Runnable() {
            @Override
            public void run() {
                load();
            }
        });
    }

    public DBListProperty(Class<T> cls) {
        this(cls, null);
    }

    private Dao<T, K> getDao() throws SQLException {
        Dao<T, K> dao = null;
        if (mTableName == null ) {
            dao = JOrmliteHelper.getSelf().getDao(mClazz);
        } else {
            DatabaseTableConfig<T> config = DatabaseTableConfigUtil.fromClass(JOrmliteHelper.getSelf().getConnectionSource(), mClazz);
            config.setTableName(mTableName);
            dao = MutilTableUtil.createDao(JOrmliteHelper.getSelf().getConnectionSource(), config);
        }
        return dao;
    }

    private void createTable()  throws SQLException {
        if (mTableName == null) {
            TableUtils.createTableIfNotExists(JOrmliteHelper.getSelf().getConnectionSource(), mClazz);
        } else {
            DatabaseTableConfig<T> config = DatabaseTableConfigUtil.fromClass(JOrmliteHelper.getSelf().getConnectionSource(), mClazz);
            config.setTableName(mTableName);
            MutilTableUtil.createTableIfNotExists(JOrmliteHelper.getSelf().getConnectionSource(), config);
        }
    }

    private void clearTable() throws  SQLException{
        if (mTableName == null) {
            TableUtils.clearTable(JOrmliteHelper.getSelf().getConnectionSource(), mClazz);
        } else {
            DatabaseTableConfig<T> config = DatabaseTableConfigUtil.fromClass(JOrmliteHelper.getSelf().getConnectionSource(), mClazz);
            config.setTableName(mTableName);
            TableUtils.clearTable(JOrmliteHelper.getSelf().getConnectionSource(), config);
        }
    }

    @Override
    public synchronized void set(List<T> value) {
        super.set(value);
        mIndexMap.clear();
        int i = 0;
        for (T obj : value) {
            mIndexMap.put(getKey(obj), i++);
        }

        final List<T> fValue = value;
        DataCenter.post(new Runnable() {
            @Override
            public void run() {

                try {

                    final Dao<T, K> dao = getDao();

                    clearTable();

                    if (fValue == null || fValue.size() == 0) {
                        return;
                    }

                    dao.callBatchTasks(new Callable<Void>() {
                        @Override
                        public Void call() throws Exception {
                            for (T obj : fValue) {
                                dao.createOrUpdate(obj);
                            }
                            return null;
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public synchronized void add(T obj) {
        K key = getKey(obj);
        Integer i = mIndexMap.get(key);

        if (i == null) {
            mIndexMap.put(key, get().size());
            super.add(obj);
        } else {
            super.set(i, obj);
        }

        final T fobj = obj;
        DataCenter.post(new Runnable() {
            @Override
            public void run() {

                try {
                    final Dao<T, K> dao = getDao();
                    dao.createOrUpdate(fobj);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        });
    }

    public synchronized T getValue(K key){
        Integer i = mIndexMap.get(key);
        if (i != null){
            return mValue.get(i);
        } else {
            return null;
        }
    }

    public synchronized void addInMainThread(T obj) {
        K key = getKey(obj);
        Integer i = mIndexMap.get(key);

        if (i == null) {
            mIndexMap.put(key, get().size());
            super.add(obj);
        } else {
            super.set(i, obj);
        }
        try {
            final Dao<T, K> dao = getDao();
            dao.createOrUpdate(obj);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public synchronized void update(T obj) {
        K key = getKey(obj);
        Integer i = mIndexMap.get(key);
        if (i != null) {
            final T fobj = obj;
            DataCenter.post(new Runnable() {
                @Override
                public void run() {

                    try {
                        final Dao<T, K> dao = getDao();
                        dao.update(fobj);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }

            });
        }
    }

    @Override
    public synchronized void remove(int i) {
        final T fobj = get().get(i);
        mIndexMap.remove(getKey(fobj));
        super.remove(i);

        DataCenter.post(new Runnable() {
            @Override
            public void run() {
                try {
                    final Dao<T, K> dao = getDao();
                    dao.delete(fobj);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });

        for (int pos = i; pos < get().size(); pos++) {
            K next = getKey(get().get(pos));
            mIndexMap.put(next, pos);
        }
    }

    public synchronized void removeByKey(final K key) {
        Integer i = mIndexMap.get(key);

        if (i == null) {
            return;
        } else {
            mIndexMap.remove(key);
            super.remove(i);
        }

        DataCenter.post(new Runnable() {
            @Override
            public void run() {
                try {
                    final Dao<T, K> dao = getDao();
                    dao.deleteById(key);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });

        for (int pos = i; pos < get().size(); pos++) {
            K next = getKey(get().get(pos));
            mIndexMap.put(next, pos);
        }
    }

    @Override
    public void remove(T obj) {
        final K key = getKey(obj);
        removeByKey(key);
    }

    @Override
    public synchronized void reset() {
    }

    @Override
    public void load() {
        try {
            createTable();
            final Dao<T, K> dao = getDao();
            set(dao.queryForAll());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int getVerison(){
        return 1;
    }

}
